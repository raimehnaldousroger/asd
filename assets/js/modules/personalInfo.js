var edit_info_modal = $("#edit-info-modal");

const personaltemp = new Vue({
	el: "#profile-template",
	data: {
		firstname: "",
		lastname: "",
		image: "",
		phone: "",
		email: "",
		customer_id: "",
		data: [],
	},
	mounted() {
		$("#edit-info-modal").hide();
	},
	methods: {
		getProfileInfo: async function () {
			$(".preference").show();
			let result = await axios.post(
				endPoints.profile + "getProfile",
				{ token: token },
				CONFIG.HEADER
			);
			let response = result.data;
			this.data = response;
			this.firstname = response.data.customer_firstname;
			this.lastname = response.data.customer_lastname;
			this.image = response.data.customer_image_url;
			this.email = response.data.customer_email;
			this.customer_id = response.data.customer_id;
			this.image = `${baseUrl}/assets/uploads/${response.data.customer_image_url}`;
		},
		selectEdit(form) {
			edit_info_modal.show();
			edit_info_modal.find("form").hide();

			let header_title = "";
			let show_form = "";

			if (form === "upload") {
				show_form = "#upload-frm";
				header_title = "Upload";
				$("#file-image").show();
				$("#start").hide();
				$("#response").show();
			}

			if (form === "name") {
				show_form = "#change-name";
				header_title = "Change name";
			}

			if (form === "pass") {
				show_form = "#change-pass";
				header_title = "Chane pass";
			}

			if (form === "email") {
				show_form = "#change-email";
				header_title = "Chane email";
			}
			editInfo.getCustomerDetails(this.data);

			edit_info_modal.find(show_form).show();
			$(".header-title").text(header_title);
		},
		close() {
			$(".profile-container").hide();
			$(".preference").hide();
			$("#live-section").show();
			$("#trending").show();
			$("#watch").show();
			$("#explore").show();
			$("#read").show();
			$("#local").show();
		},
	},
});

const editInfo = new Vue({
	el: "#edit-info",
	data: {
		image: "",
		file: "",
		customer_id: "",
		firstname: "",
		lastname: "",
		image: "",
		phone: "",
		email: "",
		password: "",
		confirm_password: "",
		new_password: "",
	},
	validators: {
		email: function (value) {
			return Validator.value(value).email(
				"That doesn't look like a valid email address."
			);
		},
	},
	methods: {
		getCustomerDetails(response) {
			this.firstname = response.data.customer_firstname;
			this.lastname = response.data.customer_lastname;
			this.image = response.data.customer_image_url;
			this.email = response.data.customer_email;
			this.customer_id = response.data.customer_id;
			this.image = `${baseUrl}/assets/uploads/${response.data.customer_image_url}`;
		},
		/*
            Submits the file to the server
        */
		submitFile() {
			/*
                Initialize the form data
            */
			let formData = new FormData();
			/*
                Add the form data we need to submit
            */
			formData.append("customer_id", this.customer_id);
			formData.append("file", this.file);
			/*
            Make the request to the POST /single-file URL
            */
			axios
				.post(
					endPoints.profile + "update_image",
					formData,
					CONFIG.HEADERUPLOAD
				)
				.then(function (response) {
					if (response.data.status !== false) {
						toastr.success(response.data.msg);
						edit_info_modal.hide();
						personaltemp.getProfileInfo();
					}
				})
				.catch(function (error) {
					console.log("FAILURE!!");
				});
		},
		/*
        Handles a change on the file upload
      */
		handleFileUpload() {
			this.file = this.$refs.file.files[0];
			$("#set-profile").attr("disabled", false);

			$("#file-image").show();
			$("#messages").html(`<span>${this.file.name}</span>`);
			this.image = URL.createObjectURL(this.file);
			$("#start").hide();
			$("#response").show();
		},
		submitForm(data, method) {
			axios
				.post(endPoints.profile + method, data, CONFIG.HEADER)
				.then(function (response) {
					if (response.data.status !== false) {
						toastr.success(response.data.msg);
						edit_info_modal.hide();
						personaltemp.getProfileInfo();
					}
				})
				.catch(function (error) {
					console.log("FAILURE!!");
				});
		},
		updateName(method) {
			let data = {
				firstname: this.firstname,
				lastname: this.lastname,
				customer_id: this.customer_id,
			};
			this.submitForm(data, method);
		},
		updatePass(method) {
			let data = {
				password: this.password,
				new_password: this.new_password,
				confirm_password: this.confirm_password,
				customer_id: this.customer_id,
			};
			this.submitForm(data, method);
		},
	},
});

// when the user click (x), close the modal
$(".modal .close").click(function (e) {
	e.preventDefault();
	edit_info_modal.hide();
});

$(".modal .cancel").click(function () {
	edit_info_modal.hide();
});
