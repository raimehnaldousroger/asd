const memberApp = new Vue({
	el: "#memberApp",
	data: {
		login: {
			username: "",
			password: "",
		},
		loginModalStatus: true,
		registerModalStatus: false,
		otpModalStatus: false,

		register: {
			firstname: "",
			lastname: "",
			email: "",
			username: "",
			password: "",
			consent: "",
		},

		message: {
			firstname: "",
			lastname: "",
			email: "",
			username: "",
			password: "",
			consent: "",
		},

		otp: {
			code: "",
		},
	},
	mounted() {
		this.isLoggin();
	},
	methods: {
		isLoggin: async function () {
			await axios
				.get(
					endPoints.member + "login" + "?token=" + token,
					CONFIG.HEADER
				)
				.then((response) => {
					userNav.setData(response.data.response);
					$(".loginDiv").addClass("d-n");
				})
				.catch((error) => {
					// localStorage.removeItem(@Token : Token ? '');
					$(".loginDiv").removeClass("d-n");
					this.loginModalStatus == true;
					// toastr.error('Session Timeout');
				});
		},
		loginBtn: function (e) {
			e.preventDefault();
			var bodyFormData = new FormData();
			bodyFormData.set("username", this.login.username);
			bodyFormData.set("password", this.login.password);

			axios
				.post(endPoints.member + "login", bodyFormData, CONFIG.HEADER)
				.then((response) => {
					token = JSON.stringify(response.data.token.access_token);
					localStorage.setItem("Token", token);
					toastr.success(
						"Welcome " +
							response.data.response.customer_firstname +
							"!"
					);
					userNav.setData(response.data.response);
					this.login.username = "";
					this.login.password = "";
					$("#loginDiv").hide();
				})
				.catch((error) => {
              		this.message.username = error.response.data.message.username
              		this.message.password = error.response.data.message.password
              		toastr.error(_.map(error.response.data.message));
				});
		},
		// hide/show of modals;
		changeModalStatus: async function () {
			if (this.loginModalStatus == true) {
				this.loginModalStatus = false;
				this.registerModalStatus = true;
				this.otpModalStatus = false;

				this.login.username = "";
				this.login.password = "";
				this.register.username = "";
				this.register.password = "";
				this.register.name = "";
				this.register.consent = "";
				this.otp.code = "";
			} else if (this.registerModalStatus == true) {
				this.loginModalStatus = true;
				this.registerModalStatus = false;
				this.otpModalStatus = false;

				this.login.username = "";
				this.login.password = "";
				this.register.username = "";
				this.register.password = "";
				this.register.name = "";
				this.register.consent = "";
				this.otp.code = "";
			} else {
				this.loginModalStatus = false;
				this.registerModalStatus = false;
				this.otpModalStatus = true;

				this.login.username = "";
				this.login.password = "";
				this.register.username = "";
				this.register.password = "";
				this.register.name = "";
				this.register.consent = "";
				this.otp.code = "";
			}
		},
		registerBtn: async function () {
			var bodyFormData = new FormData();
			bodyFormData.set("username", this.register.username);
			bodyFormData.set("password", this.register.password);
			bodyFormData.set("firstname", this.register.firstname);
			bodyFormData.set("lastname", this.register.lastname);
			bodyFormData.set("email", this.register.email);
			bodyFormData.set("consent", this.register.consent);

			await axios
				.post(
					endPoints.member + "register",
					bodyFormData,
					CONFIG.HEADER
				)
				.then((response) => {
					toastr.success("Yeah!");
					this.otpModalStatus = true;
					this.loginModalStatus = false;
					this.registerModalStatus = false;
              	})
              	.catch( (error) => {
              		this.message.firstname = error.response.data.message.firstname
              		this.message.lastname = error.response.data.message.lastname
              		this.message.email = error.response.data.message.email
              		this.message.username = error.response.data.message.username
              		this.message.password = error.response.data.message.password
              		this.message.consent = error.response.data.message.consent
					console.log(_.map(error.response.data.message) );
              	})
		},
		otpBtn: async function () {
			var bodyFormData = new FormData();
			bodyFormData.set("otp", this.otp.code);

            await axios.post(endPoints.member+'otp', bodyFormData, CONFIG.HEADER)
                 .then( (response) => {
                    toastr.success('Your Register Process is success! Please check your email for your OTP.');
                    this.loginModalStatus = true;
                    this.registerModalStatus = false;
                    this.otpModalStatus = false;
                })
                .catch( (error) => {
              		this.message.otp = error.response.data.message.otp
                })
        }
	},
});

const userNav = new Vue({
	el: "#userNav",
	data: {
		customer_firstname: "",
		customer_lastname: "",
		display: "",
		image: "",
	},
	methods: {
		setData: function (data) {
			this.customer_firstname = data.customer_firstname;
			if (data.customer_image_url != "") {
				this.image =
					server.CISERVICE +
					"uploads/display_profile/" +
					data.customer_image_url +
					".jpg";
			} else {
				this.display =
					data.customer_firstname.slice(0, 1) +
					data.customer_lastname.slice(0, 1);
			}
		},
		myProfile: function () {
			modalNav.setList();
		},
		logout: async function () {
			var bodyFormData = new FormData();
			bodyFormData.set("token", token);
			await axios
				.post(endPoints.member + "logout", bodyFormData, CONFIG.HEADER)
				.then((response) => {
					localStorage.removeItem("Token");
					this.loginModalStatus = true;
					$("#loginDiv").show();
				})
				.catch((error) => {});
		},

		showModal: function (sectionOpen) {
			personaltemp.getProfileInfo();
			$("#content section").hide();
			$("." + sectionOpen).show();
		},
	},
});

var modalNav = new Vue({
	el: "#modalNav",
	data: {
		name: "",
		email: "",

		exploreList: [],
		newsCatList: [],
		checkedCategories: "",
		PrefNewsCategoryID: [],
	},
	mounted() {},
	methods: {
		close: function (sectionOpen) {
			$("#live-section").show();
			$("#trending").show();
			$("#watch").show();
			$("#explore").show();
			$("#read").show();
			$("#local").show();
			$("#modalNav section").addClass("d-n");
		},
		setData: function (name, email, mobile) {
			this.name = name;
			this.email = email;
			this.mobile = mobile;
		},

		// get the exploreList
		setList: async function () {
			try {
				const response = await axios.get(
					endPoints.userCatPref + "categories?token=" + token,
					CONFIG.HEADER
				);

				this.exploreList = response.data.response;
				this.setUserPrefData();
				this.getNewsList();
			} catch (error) {
				console.log("Error Connecting to server");
			}
		},

		getNewsList: async function () {
			try {
				const response = await axios.get(
					endPoints.userNewsPref +
						"news_categories_list?token=" +
						token,
					CONFIG.HEADER
				);
				this.newsCatList = response.data.result;
			} catch (error) {
				console.error(error);
			}
		},

		setUserPrefData: async function () {
			try {
				const response = await axios.get(
					endPoints.userCatPref + "data?token=" + token,
					CONFIG.HEADER
				);
				this.checkedCategories = response.data.response;
				const responseNews = await axios.get(
					endPoints.userCatPref + "dataNews?token=" + token,
					CONFIG.HEADER
				);
				this.PrefNewsCategoryID = responseNews.data.response;
			} catch (error) {
				console.log("Error Connecting to server");
			}
		},
		categorySelected: function (e) {
			try {
				var bodyFormData = new FormData();
				bodyFormData.set("channel_id", this.checkedCategories);
				bodyFormData.set("news_id", this.PrefNewsCategoryID);

				axios
					.post(
						endPoints.userCatPref + "save?token=" + token,
						bodyFormData,
						CONFIG.HEADER
					)
					.then((response) => {})
					.catch((error) => {
						console.log("Error Connecting to server");
					});
			} catch {}
		},

		openModalDetails: function (formOpen) {
			$("#edit-info-modal").show();
			$("." + formOpen).removeClass("d-n");
			profileModalApp.setData(this.name, this.email, this.mobile);
		},
	},
});
