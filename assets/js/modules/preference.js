var token = JSON.parse(localStorage.getItem("Token"));
$(function () {
    var exploreApp = new Vue({
        el:'#exploreApp',
        data:{
            exploreCatList:[],
            newsCatList:[],
            PrefCategoryID: [],
            PrefNewsCategoryID: [],
        },
        mounted() {
            // this.setUserPrefData();
            // this.getNewsList();
        },
        methods: { 
            // get the exploreList
            setList: async function () {
                try {
                    const response = await axios.get(endPoints.userCatPref+'categories?token='+token, CONFIG.HEADER);
                    this.exploreCatList = response.data.result;
                } catch (error) {
                    console.log('Error Connecting to server');
                }
            },

            getNewsList: async function () {
                try {
                    const response = await axios.get(endPoints.userNewsPref+'news_categories_list?token='+token, CONFIG.HEADER);
                    this.newsCatList = response.data.result;
                } catch (error) {
                    console.error(error);
                }
            },

            setUserPrefData: async function () {
                try {
                    const response = await axios.get(endPoints.userCatPref+'data?token='+token, CONFIG.HEADER);
                    this.PrefCategoryID = response.data;
                    const responseNews = await axios.get(endPoints.userCatPref+'dataNews');
                    this.PrefNewsCategoryID = responseNews.data;
                } catch (error) {
                    console.log('Error Connecting to server');
                }
            },
            categorySelected: function (e) {
                try {
                    var bodyFormData = new FormData();
                    bodyFormData.set('channel_id', this.PrefCategoryID);
                    bodyFormData.set('news_id', this.PrefNewsCategoryID);

                    axios.post(endPoints.userCatPref+'save', bodyFormData, CONFIG.HEADER)
                        .then( (response) => {
                            
                        })
                        .catch( (error) => {
                            console.log('Error Connecting to server');
                        })
                } catch {

                }
            },
        },
    });
});