const trending = new Vue({
	el: "#trending-section",
	data: {
		now: [],
		hot: [],
		wow: [],
		videoId: "",
	},
	mounted() {
		this.setList();
		$(".top").show();
	},
	methods: {
		setList: async function () {
			let result = await axios.get(endPoints.trending + "getTrending");
			this.now = result.data.now;
			this.hot = result.data.hot;
			this.wow = result.data.wow;
		},
		playSelect: function (id) {
			this.videoId = id;
			player.destroy();
			this.initYoutube();
			mainPlayerDiv
				.removeClass("hideMainPlayerDiv")
				.addClass("showMainPlayerDiv");
		},
		initYoutube() {
			player = new YT.Player("mainPlayer", {
				height: "100%",
				width: "100%",
				videoId: this.videoId,
				playerVars: { autoplay: 1, controls: 1 },
				events: {
					onReady: this.onPlayerReady,
					onStateChange: this.onPlayerStateChange,
				},
			});
		},
		onPlayerReady(evt) {
			evt.target.playVideo();
			evt.target.mute();
		},
		onPlayerStateChange(evt) {},
		saveFavorited: function (id, type = "program") {
			let formData = new FormData();
			formData.append("id", id);
			formData.append("type", type);
			formData.append("token", token);
			axios
				.post(endPoints.favorite + "save", formData, CONFIG.HEADER)
				.then((response) => {
					if (response.data.success !== false) {
						new duDialog("", response.data.msg);
					}
				})
				.catch((error) => {
					console.log(error);
				});
		},
		share: function (id, img, type = "id") {
			let url = `${baseUrl}/?${type}=${id}`;
			$(".share-img img").attr("src", img);
			$(".share-url").val(url);
			$("#share-modal").show();
		},
	},
});

$("#mainPlayerDiv .backButton").on("click", "button", function (e) {
	$(".fa-right").show();
	player.destroy();
	mainPlayerDiv
		.removeClass("showMainPlayerDiv")
		.addClass("hideMainPlayerDiv");
	watch.initYoutube();
});
