const read = new Vue({
	el: "#read-section",
	data: {
		categories: [],
		articles: [],
		isActive: true,
		indexId: 0,
		isHidden: false,
	},
	mounted() {
		this.setCategories();
	},
	methods: {
		setCategories: async function () {
			let result = await axios.post(endPoints.read + "getCategories/");
			this.categories = result.data;
			this.getArticles(7);
		},
		getArticles: async function (id) {
			let formData = new FormData();
			formData.append("id", id);
			let result = await axios.post(
				endPoints.read + "getArticles/",
				formData
			);
			this.articles = result.data;
		},
		showArticle: async function (id, index) {
			this.indexId = index;
			this.getArticles(id);
		},
		readDetails: function (article_id) {
			details.showDetails(article_id);
		},
		showHide: function (bool) {
			this.isHidden = bool;
		},
		saveFavorited: function (id) {
			trending.saveFavorited(id, "article");
		},
		share: function (id, img) {
			trending.share(id, img, "read");
		},
	},
});

const details = new Vue({
	el: "#read-details-section",
	data: {
		banner: "",
		title: "",
		blurb: "",
		thumbnail: "",
		url: "",
	},
	mounted() {
		$("#read").show();
	},
	methods: {
		showDetails: async function (article_id) {
			$("#trending").hide();
			$("#watch").hide();
			$("#live-section").hide();
			$("#explore").hide();
			$(".link-read").click();
			$("#read").hide();

			$("#read-promp-section").show();
			let formData = new FormData();
			formData.append("article_id", article_id);
			let result = await axios.post(
				endPoints.read + "getArticle/",
				formData
			);

			let data = result.data;
			this.banner = data.banner;
			this.title = data.title;
			this.blurb = data.blurb;
			this.thumbnail = data.thumbnail;
			this.url = data.url;
		},
		close: function () {
			$("#read").show();
			$("#read-promp-section").hide();
			$("#trending").show();
			$("#watch").show();
			$("#live-section").show();
			$("#explore").show();
			$(".link-read").click();
		},
		redirect: function (url) {
			console.log(url);
		},
	},
});
