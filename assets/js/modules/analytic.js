const recentWatch = new Vue({
	el: "#recently-section",
	data: {
		favoriteList: [],
		videoId: "",
	},
	mounted() {
		this.setList();
	},
	methods: {
		setList: async function () {
			let result = await axios.get(endPoints.favorite + "show?token="+token, CONFIG.HEADER);
			this.favoriteList = result.data.response;
		},
		playSelect: function (id) {
			trending.playSelect(id);
		},

		saveFavorited: function (id, type = "program") {
			trending.saveFavorited(id);
		},
		share: function (id, img, type = "id") {
			trending.share(id, img);
		},
	},
});

$("#mainPlayerDiv .backButton").on("click", "button", function (e) {
	$(".fa-right").show();
	player.destroy();
	mainPlayerDiv
		.removeClass("showMainPlayerDiv")
		.addClass("hideMainPlayerDiv");
	watch.initYoutube();
});
