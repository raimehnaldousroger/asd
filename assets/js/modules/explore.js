const explore = new Vue({
	el: "#explore-section",
	data: {
		categories: [],
		channels: [],
	},
	mounted() {
		this.setCategories();
		$("#explore").show();
	},
	methods: {
		setCategories: async function () {
			let result = await axios.get(endPoints.explore + "getCategories");
			this.categories = result.data;
		},
		selectCategory: async function (id, name) {
			let result = await axios.get(
				endPoints.explore + "getChannels/" + id
			);
			this.channels = result.data;

			$("#trending").hide();
			$("#watch").hide();
			$("#live-section").hide();
			$("#explore").hide();

			categoryChannel.get(this.channels, name);
			$("#category-channel").show();
			$("#read").hide();
			$("#local").hide();
		},
	},
});

const categoryChannel = new Vue({
	el: "#category-channel-section",
	data: {
		items: [],
		channels: [],
		playlist: [],
		channel: "",
		playlist_title: "",
		category: "",
		type: "",
		haveRecords: false,
	},
	mounted() {
		$("#category-channel").hide();
	},
	methods: {
		get: function (data, name) {
			if (data.length > 0) {
				this.haveRecords = true;
			}
			this.items = data;
			this.channels = this.items;
			this.category = name;
			$(".link-explore").click();
		},
		show: async function (channel_id, playlist_id, title, type, video_id) {
			if (type === "channel") {
				this.playlist_title = "";
				this.channel = `> ${title}`;
				this.items = await this.getPlaylist(channel_id);
				this.playlist = this.items;
			}
			if (type === "playlist") {
				this.playlist_title = `> ${title}`;
				this.items = await this.getVideos(playlist_id);
			}
			if (type === "videos") {
				this.playlist_title = `> ${title}`;
				this.items = await this.getVideos(playlist_id);
				this.videos = this.items;
				trending.playSelect(video_id);
			}
		},
		getPlaylist: async function (id) {
			let result = await axios.post(endPoints.explore + "getPlaylist/", {
				id: id,
			});
			return result.data;
		},
		getVideos: async function (id) {
			let result = await axios.post(endPoints.explore + "getVideos/", {
				id: id,
			});
			return result.data;
		},
		showChannels: function () {
			this.items = this.channels;
			this.channel = "";
			this.playlist_title = "";
		},
		showPlaylist: function () {
			this.items = this.playlist;
			this.playlist_title = "";
		},
		backExplore: function () {
			this.haveRecords = false;
			$("#trending").show();
			$("#watch").show();
			$("#live-section").show();
			$("#explore").show();
			$(".link-explore").click();
		},
	},
});
