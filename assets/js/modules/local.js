const local = new Vue({
	el: "#local-section",
	data: {
		locations: [],
		videos: [],
	},
	mounted() {
		this.getLocation();
		$("#local").show();
	},
	methods: {
		getLocation: async function () {
			let result = await axios.get(endPoints.local + "getLocation");
			this.locations = result.data;
			this.selectLocation("all");
		},
		selectLocation: async function (id) {
			let formData = new FormData();
			formData.append("id", id);
			let result = await axios.post(
				endPoints.local + "getVideos",
				formData
			);
			this.videos = result.data;
		},
		play: function (id) {
			trending.playSelect(id);
		},
		save: function (id) {
			trending.saveFavorited(id);
		},
		share: function (id, img) {
			trending.share(id, img);
		},
	},
});
