const channeWidget = new Vue({
	el: "#channel-widget",
	data: {
		items: [],
		videoId: "",
	},
	methods: {
		toggle: async function () {
			if ($(".video-list-pannel").attr("style")) {
				$(".video-list-pannel").removeAttr("style");
			} else {
				$(".video-list-pannel").css("right", 0);
				let response = await axios.get(endPoints.watch + "getWatch");
				this.items = response.data;
			}
		},
		playSelected: function (id) {
			this.videoId = id;
			player.destroy();
			this.initYoutube();
		},
		initYoutube() {
			player = new YT.Player("live-player", {
				width: 600,
				height: 400,
				videoId: this.videoId,
				playerVars: { autoplay: 1, controls: 0 },
				events: {
					onReady: this.onPlayerReady,
					onStateChange: this.onPlayerStateChange,
				},
			});
		},
		onPlayerReady(evt) {
			evt.target.playVideo();
			evt.target.mute();
		},
		onPlayerStateChange(evt) {
			// console.log("Player state changed", evt);
		},
	},
});
