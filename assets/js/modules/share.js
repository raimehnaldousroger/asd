const share = new Vue({
	el: "#share-template-modal",
	data: {},
	methods: {
		close: function () {
			$("#share-modal").hide();
		},
		copy: function () {
			let copy_url = $(".share-url");
			copy_url.focus();
			copy_url.select();
			document.execCommand("copy");
			duDialog(null, "Link copied to clipboard", {
				okText: "Ok",
				callbacks: {
					okClick: function (e) {
						this.hide();
						$("#share-modal").hide();
					},
				},
			});
		},
	},
});

if (id !== "") {
	$("#play-share").click(function (e) {
		e.preventDefault();
		$(".white-circle").hide();
		playShared(id);
	});
	$("#play-share").click();
}

if (read_id !== "") {
	$("#show-read").click(function () {
		details.showDetails(read_id);
	});
	$("#show-read").click();
}

function playShared(id) {
	$.getScript(yt_api, function () {
		playShareVideo(id);
	});
	mainPlayerDiv
		.removeClass("hideMainPlayerDiv")
		.addClass("showMainPlayerDiv");
}

function playShareVideo(id) {
	autoplay = 1;
	controls = 1;
	window.YT.ready(function () {
		player = new window.YT.Player("mainPlayer", {
			height: "100%",
			width: "100%",
			videoId: id,
			playerVars: { autoplay: autoplay, controls: controls },
			events: {
				onReady: (event) => {
					event.target.playVideo();
					if (typeof initial !== "undefined") {
						event.target.mute();
					}
				},
				onStateChange: (event) => {
					var videoStatuses = Object.entries(window.YT.PlayerState);
					console.log(
						videoStatuses.find(
							(status) => status[1] === event.data
						)[0]
					);
				},
			},
		});
	});
}
