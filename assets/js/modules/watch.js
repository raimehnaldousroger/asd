const watch = new Vue({
	el: "#watch-section",
	data: {
		items: [],
		videoId: "",
	},
	mounted() {
		this.setWatch();
		$("#watch").show();
	},
	methods: {
		setWatch: async function () {
			try {
				this.videoId = "";
				let result = await axios.get(endPoints.watch + "getWatch", CONFIG.HEADER);
				this.items = result.data;
				this.videoId = result.data[0].video_id;
			} catch (error) {
				console.log("Error Connecting to server");
			}
		},
		selectWatch: async function (id) {
			this.videoId = id;
			player.destroy();
			this.initYoutube();
			$(".logo").click();
		},
		toggle: async function () {
			if ($(".video-list-pannel").attr("style")) {
				$(".video-list-pannel").removeAttr("style");
			} else {
				$(".video-list-pannel").css("right", 0);
				let response = await axios.get(api + "watch/getWatch", CONFIG.HEADER);
				this.items = response.data;
			}
		},
		initYoutube() {
			player = new YT.Player("live-player", {
				width: 600,
				height: 400,
				videoId: this.videoId,
				playerVars: { autoplay: 1, controls: 0 },
				events: {
					onReady: this.onPlayerReady,
					onStateChange: this.onPlayerStateChange,
				},
			});
		},
		onPlayerReady(evt) {
			evt.target.playVideo();
			evt.target.mute();
		},
		onPlayerStateChange(evt) {
			// console.log("Player state changed", evt);
		},
	},
});

onYouTubeIframeAPIReady = () => {
	watch.initYoutube();
};
