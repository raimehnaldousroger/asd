var getUrl = window.location;
var baseUrl =
	getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split("/")[1];
	console.log(baseUrl);
var server = {
	CISERVICE: baseUrl + "/iptv_api/",
};

var viewRoutes = {
	// 'HOME'               		  : server.CISITE + '/',
	// 'LOGIN'                       : server.CISITE + '/signin',
	// 'LOGOUT'                      : server.CISITE + '/logout',
	// 'REGISTER'                    : server.CISITE + '/registration',
};

var endPoints = {
	watch: server.CISERVICE + "watch/",
	trending: server.CISERVICE + "trending/",
	member: server.CISERVICE + "site/",
	favorite: server.CISERVICE + "favorite/",
	explore: server.CISERVICE + "explore/",
	read: server.CISERVICE + "read/",
	local: server.CISERVICE + "local/",
	userCatPref: server.CISERVICE + "preference/",
	userNewsPref: server.CISERVICE + "article/",
	favortite: server.CISERVICE + "favorite/",
	profile: server.CISERVICE + "profile/",
};

/** LOCAL WEB STORAGE ITEMS **/
var STORAGE_ITEM = {
	TOKEN: "Token",
};

var CONFIG = {
	HEADER: {
		headers: {
			Authorization:
				"Basic " + btoa("NowNaPortal" + ":" + "N0V/N@p0Rt@l"),
			"X-API-KEY": "4cww48g0cggc4kgggsckg8wo4kk8k8wowwgooo44",
		},
	},
	HEADERUPLOAD: {
		headers: {
			"Content-Type": "multipart/form-data",
			Authorization:
				"Basic " + btoa("NowNaPortal" + ":" + "N0V/N@p0Rt@l"),
			"X-API-KEY": "4cww48g0cggc4kgggsckg8wo4kk8k8wowwgooo44",
		},
	},
};
