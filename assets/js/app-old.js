var mainPlayerDiv = $("#mainPlayerDiv");
var mainPlayer = $("#mainPlayer");
var iframe = mainPlayerDiv.find("iframe");
const current_url = window.location.href;
const split_url = current_url.split("?");

var yt_main_player;

function loadMainVideo(id) {
	autoplay = 1;
	controls = 1;
	window.YT.ready(function () {
		yt_main_player = new window.YT.Player("mainPlayer", {
			height: "100%",
			width: "100%",
			videoId: id,
			playerVars: { autoplay: autoplay, controls: controls },
			events: {
				onReady: (event) => {
					event.target.playVideo();
				},
				onStateChange: (event) => {
					var videoStatuses = Object.entries(window.YT.PlayerState);
					console.log(
						videoStatuses.find((status) => status[1] === event.data)[0]
					);
				},
			},
		});
	});
}

if (typeof Storage === "undefined") {
	console.error("Browser does not support Web Storage...");
}

function isLoggin() {
	return localStorage.getItem("member_logged_in_status");
}

function getLoggedIn(fieldname) {
	return localStorage.getItem(fieldname);
}

const cardClickHandler = (elem) => {
	try {
		var src = elem.data("url");
		$(".white-circle").hide();
		mainPlayer.show();

		/** Added Kenneth 20201028 */
		if (elem.attr("class") == "thumbnail") {
			showReadPrompt(elem.data("id"));
		} else {
			var tile_type = 2;
			addAnalytics(elem, tile_type);

			yt_live_player.pauseVideo();
			var src_embed = elem.data("embedded_url");
			src = getValidURL(src, src_embed);
			$.getScript(yt_api, function () {
				loadMainVideo(youtube_parser(src));
			});
		} // Added Kenneth 20201028
		mainPlayerDiv
			.removeClass("hideMainPlayerDiv")
			.addClass("showMainPlayerDiv");
	} catch (e) {
		console.log(e);
	}
};

var addAnalytics = function (_parent, tile_type) {
	var parent_id = _parent.attr("data-id");
	var type = tile_type;

	// var member_id = $.session.get("member_id");
	// var user_client = $.session.get("user_client");
	// var ip_address = $.session.get("ip_address");
	var member_id = getLoggedIn("member_id");
	var user_client = getLoggedIn("user_client");
	var ip_address = getLoggedIn("ip_address");
	var tag = _parent.data("tags");
	var url = "/v1/iptv_api/v1/add_analytics/";

	$.post(
		url,
		{
			member_id: member_id,
			parent_id: parent_id,
			type: type,
			user_client: user_client,
			ip_address: ip_address,
			tag: tag,
		},
		function (data) {}
	);
};

/**
 * Hide and Show Main Player
 */

$(function () {
	$("#watch").on("click", ".cards", function (e) {
		cardClickHandler($(this));
	});

	$("#trending").on("click", ".cards", function (e) {
		cardClickHandler($(this));
	});

	/** Added Kenneth 20201028 */
	$("#read").on("click", ".thumbnail", function (e) {
		cardClickHandler($(this));
	});
	/** End Kenneth 20201028 */

	$("#local").on("click", ".card", function (e) {
		cardClickHandler($(this));
	});

	// when the user click span(close), hide the favorited section
	$("section").on("click", ".close", function (e) {
		e.preventDefault();
		$(".live").show();
		$(".top").show();
		$(".read").show();
		$(".local").show();
		$("#favorite-section").hide();
		$("#recently-section").hide();
		$(".profile-container").hide();
		$(".recently-read-section").hide();
		$(".read-promp-section").hide();
	});

	$("#mainPlayerDiv .backButton").on("click", "buttons", function (e) {
		$(".fa-right").show();
		mainPlayerDiv
			.removeClass("showMainPlayerDiv")
			.addClass("hideMainPlayerDiv");
		yt_live_player.playVideo();
		yt_main_player.destroy();
	});
});

/**
 * Close Details dialog box
 */
$(".live").on("click", ".close", function () {
	$(".live .details").hide("slow");
});

const getValidURL = (src, src_embed = "") => {
	var embed_url = "www.youtube.com/embed/";
	if (src_embed) {
		src = src_embed;
	}
	// src = 'https://www.youtube.com/watch?v=m6NwF9TM9is&feature=youtu.be';
	// console.log(src);
	var src_val = src.split("?");
	var src_ar = src_val[0].split("/");
	var protocol = src_ar[0] + "//";
	if (src_val[0].indexOf("youtu.be") >= 0) {
		src = protocol + embed_url + src_ar[src_ar.length - 1];
	} else if (
		src_val[0].indexOf("youtube.com") >= 0 &&
		src.indexOf("youtube.com/embed") == -1
	) {
		var src_ar_param = src_val[1].split("&");
		for (var c = 0; c < src_ar_param.length; c++) {
			var param = src_ar_param[c].split("=");
			if ((param[0] = "v")) {
				src = protocol + embed_url + param[1];
				c = src_ar_param.length;
			}
		}
	}
	return src;
};

/** Start - junrel 10 / 29 / 2020
 *
 * adding css to section and page animation
 * */
// $("a.page-link").on("click", function (event) {
// 	// this line prevents the default behaviour (jump to link) as we are going to do that
// 	event.preventDefault();

// 	// $(".category-channel .wrapper-channel").html(" ");
// 	$(".top").show();
// 	$(".live").show();
// 	$(".read").show();
// 	$(".local").show();
// 	$(".category-channel").hide();
// 	$(".cat-name").html(" ");
// 	$(".channel-title").html(" ");
// 	$(".bread-camp-left").html(" ");

// 	let id = $(this).attr("href");

// 	activeLink($(this));

// 	let click_search = $(".icon-search").attr("data-click");
// 	if (click_search == 1) {
// 		$(".icon-search").attr("data-click", 1);
// 	}
// 	$(".search-container").hide();
// 	$("#favorite-section").hide();
// 	$("#recently-section").hide();
// 	$("#favorite-section-article").hide();
// 	$(".profile-container").hide();
// 	$(".recently-read-section").hide();
// 	$(".dropdown-content").removeClass("show");
// 	$(".icon-search").find("span.close").hide();
// 	$(".icon-search").find(".search-svg").show();

// 	$("ul").removeClass("open");
// 	$(".burger").removeClass("toggle");

// 	// if the section is not the banner, add padding top to it.
// 	if ($(this).hasClass("logo")) {
// 		$(id).css("padding-top", "  0px");
// 	} else {
// 		$(id).css("padding-top", "50px");
// 	}

// 	// animate scrolling body
// 	scrollTop(this);
// });

function activeLink(_this) {
	$("a.page-link").removeAttr("style");
	if (_this) {
		_this.css("color", "#0ab39f");
	}
}

$(".logo").click(function () {
	// animate scrolling body
	scrollTop(this);
});
// end - junrel 10/29/2020

/** Start - junrel 10 / 30 / 2020
 *
 * css preloader template
 * */
function imagePreloader() {
	return `<div class="lds-ellipsis">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>`;
}
// end - junrel 10/29/2020

/** Start - junrel 11 / 3 / 2020
 *
 * navigation toggle
 * */
$(".burger").click(function () {
	$(".dropdown-content").removeClass("show");
	if ($("ul").hasClass("open")) {
		$("ul").removeClass("open");
		$(this).removeClass("toggle");
	} else {
		$("ul").addClass("open");
		$(this).addClass("toggle");
	}
});

function playShared(id) {
	$.getScript(yt_api, function () {
		playShareVideo(id);
	});
	mainPlayerDiv.removeClass("hideMainPlayerDiv").addClass("showMainPlayerDiv");
}

let socials = ["facebook", "twitter", "instagram", "youtube"];

function addActive(social) {
	for (let i = 0; i < socials.length; i++) {
		if (socials[i] == social) {
			$(`.fa-${socials[i]}`).css("color", "#00a693");
		} else {
			$(`.fa-${socials[i]}`).removeAttr("style");
		}
	}
}

function playShareVideo(id) {
	autoplay = 1;
	controls = 1;
	window.YT.ready(function () {
		yt_main_player = new window.YT.Player("mainPlayer", {
			height: "100%",
			width: "100%",
			videoId: id,
			playerVars: { autoplay: autoplay, controls: controls },
			events: {
				onReady: (event) => {
					event.target.playVideo();
					if (typeof initial !== "undefined") {
						event.target.mute();
					}
				},
				onStateChange: (event) => {
					var videoStatuses = Object.entries(window.YT.PlayerState);
					console.log(
						videoStatuses.find((status) => status[1] === event.data)[0]
					);
				},
			},
		});
	});
}

// check if empty object
function isEmpty(obj) {
	return Object.keys(obj).length === 0;
}

// animate scrolling body
function scrollTop(_this) {
	$("html, body").animate(
		{
			// we are animating the "scrollTop" DOM property to the top offset position of the named anchor
			// that matches the "href" attribute of the element that was clicked
			scrollTop: $($.attr(_this, "href")).offset().top,
		},
		600
	); // time to take animating (500 milliseconds)
}

// parse url youtube
function parse_url(url) {
	var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = url.match(regExp);

	if (match && match[2].length == 11 && match[2] != "videoseries") {
		return match[2];
	} else {
		return false;
	}
}
// end

function getUrlParameter(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	var results = regex.exec(location.search);
	return results === null
		? ""
		: decodeURIComponent(results[1].replace(/\+/g, " "));
}
