var player;
var mainPlayerDiv = $("#mainPlayerDiv");

const yt_api = "https://www.youtube.com/iframe_api";

var token = JSON.parse(localStorage.getItem("Token"));

var id = getUrlParameter("id");

var read_id = getUrlParameter("read");

$("a.page-link").on("click", function (event) {
	// this line prevents the default behaviour (jump to link) as we are going to do that
	event.preventDefault();

	$(".top").show();
	$(".live").show();
	$("#read").show();
	$(".local").show();
	$(".category-channel").hide();

	let id = $(this).attr("href");

	activeLink($(this));

	let click_search = $(".icon-search").attr("data-click");
	if (click_search == 1) {
		$(".icon-search").attr("data-click", 1);
	}
	$(".search-container").hide();
	$("#favorite-section").hide();
	$("#recently-section").hide();
	$("#favorite-section-article").hide();
	$(".profile-container").hide();
	$(".recently-read-section").hide();
	$(".dropdown-content").removeClass("show");
	$(".icon-search").find("span.close").hide();
	$(".icon-search").find(".search-svg").show();

	$("ul").removeClass("open");
	$(".burger").removeClass("toggle");

	// if the section is not the banner, add padding top to it.
	if ($(this).hasClass("logo")) {
		$(id).css("padding-top", "  0px");
	} else {
		$(id).css("padding-top", "50px");
	}

	// animate scrolling body
	scrollTop(this);
});

$(".logo").click(function () {
	// animate scrolling body
	scrollTop(this);
});

$(".link-explore").click(function () {
	let id = $(this).attr("href");
	$(id).css("padding-top", "100px");
	scrollTop(this);
});

$(".link-read").click(function () {
	let id = $(this).attr("href");
	$(id).css("padding-top", "100px");
	scrollTop(this);
});

/**
 * Close Details dialog box
 */
$(".live").on("click", ".close", function () {
	$(".live .details").hide("slow");
});

function activeLink(_this) {
	$("a.page-link").removeAttr("style");
	if (_this) {
		_this.css("color", "#0ab39f");
	}
}

// animate scrolling body
function scrollTop(_this) {
	$("html, body").animate(
		{
			// we are animating the "scrollTop" DOM property to the top offset position of the named anchor
			// that matches the "href" attribute of the element that was clicked
			scrollTop: $($.attr(_this, "href")).offset().top,
		},
		600
	); // time to take animating (500 milliseconds)
}

function getUrlParameter(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	var results = regex.exec(location.search);
	return results === null
		? ""
		: decodeURIComponent(results[1].replace(/\+/g, " "));
}
