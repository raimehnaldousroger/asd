
		<div id="fb-root"></div>
		<script
			async
			defer
			crossorigin="anonymous"
			src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0&appId=4905457216194271&autoLogAppEvents=1"
			nonce="hkfmDzBC"
		></script>
		<header>
			<nav>
				<a href="#live" class="logo page-link">
					<img src="<?= base_url('assets/images/NOW NA LOGO_White.png') ?>" alt="NOWNa"/>
				</a>
				<ul class="nav-links">
					<li><a href="#trending" class="page-link">Trending</a></li>
					<li><a href="#watch" class="page-link">Watch</a></li>
					<li><a href="#explore" class="page-link link-explore">Explore</a></li>
					<li><a href="#read" class="page-link link-read">Read</a></li>
					<li><a href="#local" class="page-link">Local</a></li>
					<li class="log-mobile d-n">
						<a @click="logout()">Logout</a>
					</li>
				</ul>
				<div class="search-wrapper-mobile d-n">
					<button
						type="submit"
						class="icon-search"
						name="search"
						data-click="1"
					>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="18"
							height="18"
							style="fill: #ffffff"
							class="search-svg"
						>
							<path
								d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z"
							></path>
						</svg>
						<span class="close d-n"
							><svg
								viewBox="0 0 24 24"
								preserveAspectRatio="xMidYMid meet"
								focusable="false"
								class="style-scope yt-icon"
								style="
									pointer-events: none;
									display: block;
									width: 100%;
									height: 100%;
									fill: #0ab39f;
									width: 25px;
								"
							>
								<g class="style-scope yt-icon">
									<path
										d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
										class="style-scope yt-icon"
									></path>
								</g></svg
						></span>
					</button>
				</div>
				<div
					class="profileLinks-mobile d-n profileLinks"
					id="profileLinks"
				></div>
				<ul class="login" id="userNav">
					<div class="search-wrapper search-wrapper-desktop">
						<button
							type="submit"
							class="icon-search"
							name="search"
							data-click="1"
							@click="showModal('search-container')"
						>
						<i class="fas fa-search"></i>
							<span class="close d-n"><svg
									viewBox="0 0 24 24"
									preserveAspectRatio="xMidYMid meet"
									focusable="false"
									class="style-scope yt-icon"
									style="
										pointer-events: none;
										display: block;
										width: 100%;
										height: 100%;
										fill: #0ab39f;
										width: 25px;
									"
								>
									<g class="style-scope yt-icon">
										<path
											d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
											class="style-scope yt-icon"
										></path>
									</g></svg
							></span>	
						</button>
					</div>
					<li id="profileLinks" class="profileLinks-d profileLinks" >
						<div class="dropdown">
							<a v-if=" image != '' " v-bind:style="{ 'background-image': 'url(' + image + ')' }" style="background-size: cover;padding: 8px 20px;margin: 8px;"></a>
							<a v-if=" display != '' " style="background-size: cover;padding: 8px 12px;margin: 8px;border-radius: 20px;background-image: linear-gradient(#00a693, #d0dd37);text-shadow: 1px 1px black;">{{display}}</a>
							<div class="dropdown-content">
								<a id="hi" href="javascript:void(0)" class="" style=color:#bfbfbf;>Hi {{customer_firstname}}!</a>
						        <a id="profile" class="profile" @click="showModal('profile-container');">My Profile</a>
								<a id="recently-watch" class="recently-watch" @click="showModal('recently-section')">Recently Watched</a>
								<a id="recently-read" class="recently-read" @click="showModal('recently-read-section')">Recently Read</a>
								<a id="favorited" class="favorited" @click="showModal('favorite-section')">Favorited</a>
						    </div>
						</div>
					</li>
					<li id="loginLinks">
						<a @click="logout()" >Logout</a>
					</li>
				</ul>
				<div class="burger">
					<div class="line1"></div>
					<div class="line2"></div>
					<div class="line3"></div>
				</div>
			</nav>
		</header>
		<div id="content" class="container">
			<?php if(!isset($_COOKIE['accept-cookies'])) { ?>
				<!-- <div class="cookie-banner">
					<p>The NOW Corp and NOWaccount web sites may use “cookies” to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. <a href="?accept-cookies" class="close">Accept</a> </p>
				</div> -->
			<?php } ?>
			<!-- Live ====================================s-->
			<section class="live-section" id="live-section">
				<div class="live" id="live">
					<!-- <iframe
					id="iframe"
					width="100%"
					height="100%"
					frameborder="0"
					allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
					style="display: none"
				></iframe> -->
					<!-- youtube banner -->
					<div id="live-player" class="live-player"></div>
					<!-- end -->
					<div class="social">
						<button
							type="button"
							class="btn-rss btn-social-desktop"
						>
							<i class="fas fa-rss arrow-desktop"></i>
						</button>
						<button
							type="button"
							class="btn-rss btn-social-mobile d-n"
						>
							<i class="fas fa-rss"></i>
						</button>
					</div>

					<div class="details">
						<div class="modal-header">
							<button
								type="button"
								class="close"
								data-dismiss="modal"
								aria-label="Close"
							>
								<span aria-hidden="true"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
											fill: #0ab39f;
											width: 25px;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</button>
						</div>
						<div class="content-wrapper">
							<p class="notice">Livestreaming NOW!</p>
							<p class="headline">
								(At the moment: Typhoon enters PH
								responsibility,
								<span class="source">News from GMA7</span>)
							</p>
							<!-- <iframe src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2FIamLadiGabster%2Fvideos%2F835320197248161%2F&show_text=false&width=560" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe> -->
							<!-- Add a placeholder for the Twitch embed -->
							<!-- <div id="twitch-embed"></div> -->

							<!-- Load the Twitch embed script -->
							<!-- <script src="https://player.twitch.tv/js/embed/v1.js"></script> -->

							<!-- Create a Twitch.Player object. This will render within the placeholder div -->
							<!-- <script type="text/javascript">
							  new Twitch.Player("twitch-embed", {
							    channel: "ufc"
							  });
							</script> -->
						</div>

						<div class="video-wrapper">
							  <iframe
								class="livestream-iframe"
								width="450"
								height="340"
								src=""
								frameborder="0"
								allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							>
							</iframe>
							<!-- <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script> -->

							  <!-- Your embedded video player code -->
							  <!-- <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/892838968192245/" data-width="500" data-show-text="false" data-autoplay="true"></div> -->
						</div>
						<button
							type="button"
							class="watch-here"
							name="watch-here"
							id="watch-here"
						>
							Watch here
						</button>
					</div>
					<div class="video-list-pannel" id="channel-widget">
						<div class="video-arrow" id="video-arrow-d" @click="toggle()">
							<div
								class="VideoLandingPage-arrow-2bsv8"
								tabindex="0"
							>
								<svg
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 363.6"
								>
									<path
										d="M0 363.6h178.1V207.8H0v155.8zm55.5-119.2l71.8 41.4-71.8 41.4v-82.8zm374.4 19.2h-185v-22.3h185v22.3zm9.9-205H244.9V35.1h194.9v23.5zm72.2 41.5v22.3H244.9v-22.3H512zM244.9 308H512v22.3H244.9V308zM0 155.8h178.1V0H0v155.8zM55.5 36.5l71.8 41.4-71.8 41.4V36.5z"
									></path>
								</svg>
							</div>
						</div>
						<div class="video-arrow d-n" id="video-arrow-m">
							<div
								class="VideoLandingPage-arrow-2bsv8"
								tabindex="0"
							>
								<svg
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 363.6"
								>
									<path
										d="M0 363.6h178.1V207.8H0v155.8zm55.5-119.2l71.8 41.4-71.8 41.4v-82.8zm374.4 19.2h-185v-22.3h185v22.3zm9.9-205H244.9V35.1h194.9v23.5zm72.2 41.5v22.3H244.9v-22.3H512zM244.9 308H512v22.3H244.9V308zM0 155.8h178.1V0H0v155.8zM55.5 36.5l71.8 41.4-71.8 41.4V36.5z"
									></path>
								</svg>
							</div>
						</div>
						<div class="title">
							<h2>Watch</h2>
							<span aria-hidden="true" class="close-watch" @click="toggle()"
								><svg
									viewBox="0 0 24 24"
									preserveAspectRatio="xMidYMid meet"
									focusable="false"
									class="style-scope yt-icon"
									style="
										pointer-events: none;
										display: block;
										width: 100%;
										height: 100%;
										fill: #0ab39f;
										width: 25px;
									"
								>
									<g class="style-scope yt-icon">
										<path
											d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
											class="style-scope yt-icon"
										></path>
									</g></svg
							></span>
						</div>
						<div class="channel-list">
							  <div class="video" v-for="item in items">
								<img :src="item.thumbnail" alt="" srcset="">
								<div class="description" @click="playSelected(item.video_id)">
									<p class="channel">{{item.channel_no}} | {{item.channel_title}}</p>
									<p>{{item.description}}</p>
								</div>
							</div>
						</div>
						<div class="close-watch-list">
							<span class="close" style="width: 25px"
								><svg
									viewBox="0 0 24 24"
									preserveAspectRatio="xMidYMid meet"
									focusable="false"
									class="style-scope yt-icon"
									style="
										pointer-events: none;
										display: block;
										width: 100%;
										height: 100%;
									"
								>
									<g class="style-scope yt-icon">
										<path
											d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
											class="style-scope yt-icon"
										></path>
									</g></svg
							></span>
						</div>
					</div>
				</div>
				<div class="playlist-wrapper">
					<div class="title">
						<h2>Social</h2>
						<span aria-hidden="true" class="close-playlist"
							><svg
								viewBox="0 0 24 24"
								preserveAspectRatio="xMidYMid meet"
								focusable="false"
								class="style-scope yt-icon"
								style="
									pointer-events: none;
									display: block;
									width: 100%;
									height: 100%;
									fill: #0ab39f;
									width: 25px;
								"
							>
								<g class="style-scope yt-icon">
									<path
										d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
										class="style-scope yt-icon"
									></path>
								</g></svg
						></span>
					</div>
					<div class="social">
						<button
							type="button"
							class="btn-social facebook-list"
							data-icon="facebook"
						>
							<i class="fab fa-facebook"></i>
						</button>
						<button
							type="button"
							class="btn-social twitter-list"
							data-icon="twitter"
						>
							<i class="fab fa-twitter"></i>
						</button>
						<button
							type="button"
							class="btn-social instagram-list"
							data-icon="instagram"
						>
							<i class="fab fa-instagram"></i>
						</button>
						<button
							type="button"
							class="btn-social youtube-list"
							data-icon="youtube"
						>
							<i class="fab fa-youtube"></i>
						</button>
					</div>
					<div class="playlist"></div>
				</div>
			</section>

			<!-- start profile ================================ -->
			<template id="profile-template">
				<section class="profile-container d-n">
					<div class="profile-wrapper">
						<div class="title">
							<span class="logged"></span>
							<h2>Personal info</h2>
							<span class="close" @click="close" style="width: 25px"
								><svg
									viewBox="0 0 24 24"
									preserveAspectRatio="xMidYMid meet"
									focusable="false"
									class="style-scope yt-icon"
									style="
										pointer-events: none;
										display: block;
										width: 100%;
										height: 100%;
									"
								>
									<g class="style-scope yt-icon">
										<path
											d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
											class="style-scope yt-icon"
										></path>
									</g></svg
							></span>
						</div>

						<div class="card">
							<div class="card-title"><h2>Basic info</h2></div>
							<input type="hidden" name="prof-id" id="prof-id" />
							<div class="divider" @click="selectEdit('upload')">
								<div class="text text-mobile">
									<h3 class="divider-name">Photo</h3>
								</div>
								<div class="text-desc avatar-info">Add a photo</div>
								<div class="photo-wrapper">
									<div class="photo avatar" :style="{backgroundImage:`url(${image})`}" style="background-size:cover">
										<h2 class="avatar-name">J</h2>
										<div class="photo-icon">
											<i class="fas fa-camera"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="border-top"></div>
							<div class="divider name" @click="selectEdit('name')">
								<div class="text text-mobile">
									<h3 class="divider-name">Name</h3>
								</div>
								<div class="text name-info">{{firstname}} {{lastname}}</div>
								<div class="text edit">
									<svg
										aria-hidden="true"
										focusable="false"
										data-prefix="fal"
										data-icon="pencil"
										role="img"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 512 512"
										class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
									>
										<path
											fill="currentColor"
											d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
											class=""
										></path>
									</svg>
								</div>
							</div>
							<div class="border-top"></div>
							<div class="divider" @click="selectEdit('pass')">
								<div class="text text-mobile">
									<h3 class="divider-name">Password</h3>
								</div>
								<div class="pass password-info">••••••••</div>
								<div class="text edit">
									<svg
										aria-hidden="true"
										focusable="false"
										data-prefix="fal"
										data-icon="pencil"
										role="img"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 512 512"
										class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
									>
										<path
											fill="currentColor"
											d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
											class=""
										></path>
									</svg>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-title"><h2>Contact info</h2></div>

							<div class="divider email" @click="selectEdit('email')">
								<div class="text text-mobile">
									<h3 class="divider-name">Email</h3>
								</div>
								<div class="text email-info">
									{{email}}
								</div>
								<div class="text edit">
									<svg
										aria-hidden="true"
										focusable="false"
										data-prefix="fal"
										data-icon="pencil"
										role="img"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 512 512"
										class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
									>
										<path
											fill="currentColor"
											d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
											class=""
										></path>
									</svg>
								</div>
							</div>
							<!-- <div class="border-top"></div>
							<div class="divider phone" element="mobile">
								<div class="text text-mobile">
									<h3 class="divider-name">Phone</h3>
								</div>
								<div class="text-desc contact-info">0912345678</div>
								<div class="text edit">
									<svg
										aria-hidden="true"
										focusable="false"
										data-prefix="fal"
										data-icon="pencil"
										role="img"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 512 512"
										class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
									>
										<path
											fill="currentColor"
											d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
											class=""
										></path>
									</svg>
								</div>
							</div> -->
						</div>
					</div>
				</section>
			</template>
			
			<!-- end profile ================================-->

			<!-- vuejs retrieve categories to be choose and save user's preffered video - RAI -->
			<section class="preference d-n" id="modalNav">
				<div class="card" style="width:60%;margin:auto;" >
					<div class="card-title"><h2>My Preference</h2></div>	
					<div id="preferences">
						<div class="videoCat">
							<h1>Video Category</h1>
							<ul>
								<li v-for="explore in exploreList">
									<input type="checkbox" :id="'cb'+explore.id" :name="explore.name" :value="explore.id" v-model="checkedCategories" @change="categorySelected($event)">
									<label :for="'cb'+explore.id"><img :src="baseUrl+'/assets/images/category/'+explore.name+'.jpg'"></label>
									<p>{{explore.name}}</p>
								</li>
							</ul>
						</div>
						<div class="news">
							<h1>News Category</h1>
							<div v-for="newsCatList in newsCatList">
								<input type="checkbox" :id="'news'+newsCatList.id" v-model="PrefNewsCategoryID" :name="newsCatList.name" :value="newsCatList.id" @change="categorySelected($event)" >
								<label :for="'news'+newsCatList.id">{{newsCatList.name}}</label>
							</div>
						</div>
						<!-- <span style="color: #000;">{{checkedCategories}}</span> -->
					</div>
				</div><!-- end -->
			</section>

			<!-- recently watched ================================ -->
			<section class="recently-section pt-150 d-n" id="recently-section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Recently watched</h4>
								<span class="close" @click="close('recently-section')">
									<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope yt-icon" style=" pointer-events: none; display: block; width: 100%; height: 100%;">
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path></g>
									</svg>
								</span>
							</div>l
							<button type="button" class="clear-history">
								<i class="fas fa-history"></i> Clear
							</button>
							<div class="video-list">
								<div class="card" v-for="item in favoriteList" @click="playSelect(item.video_id)">
									<div class="thumbnail">
										<img alt="picture" :src="item.thumbnail" class="img-fluid">
									</div>

									<div class="content">
										<h2>{{item.title}}</h2>
										<div class="action">
											<button class="remove" data-type="video" id="1">
												<i class="fas fa-trash"></i>
											</button>
											<button class="share btn" @click.stop.prevent="share(item.video_id, item.thumbnail)" id="share" type="button" name="share"><i class="fas fa-share"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--End recently watch ================================ -->

			<!-- recently read ================================ -->
			<section
				class="recently-read-section pt-150 d-n"
				id="recently-read-section"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Recently read</h4>
								<span class="close" @click="close('recently-read-section')"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</div>
							<button type="button" class="clear-history">
								<i class="fas fa-history"></i> Clear
							</button>
							<div class="video-list"></div>
						</div>
					</div>
				</div>
			</section>

			<!--End recently read ================================ -->

			<!-- Favorite ================================ -->
			<section
				class="favorite-section fav-section pt-150 d-n"
				id="favorite-section"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Favorited</h4>

								<span class="close" @click="close('favorite-section')"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</div>

							<div class="video-list">
								<div class="card" v-for="item in favoriteList" @click="playSelect(item.video_id)">
									<div class="thumbnail">
										<img alt="picture" :src="item.thumbnail" class="img-fluid">
									</div>

									<div class="content">
										<h2>{{item.title}}</h2>
										<div class="action">
											<button class="remove" data-type="video" id="1">
												<i class="fas fa-trash"></i>
											</button>
											<button class="share btn" @click.stop.prevent="share(item.video_id, item.thumbnail)" id="share" type="button" name="share"><i class="fas fa-share"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--End Favorite ================================ -->

			<!-- Favorite ================================ -->
			<section
				class="favorite-section-article fav-section d-n"
				id="favorite-section-article"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Read</h4>
								<!-- <span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span> -->
							</div>

							<div class="list-article"></div>
						</div>
					</div>
				</div>
			</section>
			<!--End Favorite ================================ -->

			<!--Start Search ================================ -->
			<section class="search-container d-n" id="search">
				<div class="search-wrapper">
					<div class="search-input">
						<input
							type="text"
							name="search"
							id="search"
							autofocus
							placeholder="Search...."
						/>
						<button type="submit" class="btn-search" name="search">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="18"
								height="18"
							>
								<path
									fill="rgb(151,151,159)"
									d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z"
								></path>
							</svg>
						</button>
					</div>
					<div class="results-container">
						<div class="results-wrapper">
							<div class="results"></div>
							<div class="spinner">
								<i class="fas fa-circle-notch fa-spin"></i>
							</div>
							<button class="btn-load d-n">LOAD MORE</button>
						</div>
					</div>
				</div>
				<div class="results-read-wrapper d-n">
					<div class="wrapper">
						<h3 class="title-result">Read</h3>
						<div class="results"></div>
						<div class="full-read">
							<button class="back">
								<i class="fa fa-times"></i>
							</button>
							<h3 class="title"></h3>
							<div class="img-banner"></div>
							<p class="content"></p>
							<div class="btn-group">
								<svg
									width="19"
									height="13"
									viewBox="0 0 19 13"
									fill="none"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										d="M18 5L9.5 12L1 5"
										stroke="#009688"
										stroke-width="1.5"
									></path>
									<path
										d="M18 1L9.5 8L1 1"
										stroke="#009688"
										stroke-width="1.5"
									></path>
								</svg>
								<button class="full-story">
									Read full story
								</button>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--end Search ================================ -->
			<section class="top">
				<div id="play-share"></div>
				<template id="trending-section">
					<div class="trending" id="trending">
						<h3>Trending</h3>
						<h4>NOW</h4>
						<div class="now-wrapper">
							<div class="now">
								<div v-for="item in now" class="card" :style="{backgroundImage:`url(${item.thumbnail})`}">
									<div class="card-body" @click="playSelect(item.video_id)">
										<div class="items">
											<p>{{item.title}}</p>
											<div class="action">
												<button class="favorite btn" @click.stop.prevent="saveFavorited(item.id)" type="button" name="favorite"><i class="fas fa-star"></i></button>
												<button class="share btn" @click.stop.prevent="share(item.video_id, item.thumbnail)" id="share" type="button" name="share"><i class="fas fa-share"></i></button>
											</div>
										</div>
									</div>
								</div>
							<div
								class="fa-left white-circle btn-prev left-now"
								data-element="now"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-left"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
									></path>
								</svg>
							</div>
							<div
								class="fa-right white-circle btn-next right-now"
								data-element="now"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-right"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
									></path>
								</svg>
							</div>
						</div>
						<h4>HOT</h4>
						<div class="now-wrapper">
							<div class="hot">
								<div v-for="item in hot" data-embedded_url="https://www.youtube.com/watch?v=KsN5YbbWURI" data-url="https://www.youtube.com/embed/KsN5YbbWURI" class="card" :style="{backgroundImage:`url(${item.thumbnail})`}">
									<div class="card-body" @click="playSelect(item.video_id)">
										<div class="items">
											<p>{{item.title}}</p>
											<div class="action">
												<button class="favorite btn" type="button" name="favorite" @click.stop.prevent="saveFavorited(item.channel_id)"><i class="fas fa-star"></i></button>
												<button class="share btn" @click.stop.prevent="share(item.video_id, item.thumbnail)" id="share" type="button" name="share"><i class="fas fa-share"></i>
											</button></div>
										</div>
									</div>
								</div>
							</div>
							<div
								class="fa-left white-circle btn-prev d-n left-hot"
								data-element="hot"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-left"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
									></path>
								</svg>
							</div>
							<div
								class="fa-right white-circle btn-next right-hot"
								data-element="hot"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-right"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
									></path>
								</svg>
							</div>
						</div>
						<h4>WOW</h4>
						<div class="now-wrapper">
							<div class="wow">
								<div v-for="item in wow" data-embedded_url="https://www.youtube.com/watch?v=KsN5YbbWURI" data-url="https://www.youtube.com/embed/KsN5YbbWURI" class="card" :style="{backgroundImage:`url(${item.thumbnail})`}">
									<div class="card-body" @click="playSelect(item.video_id)">
										<div class="items">
											<p>{{item.title}}</p>
											<div class="action">
												<button class="favorite btn" type="button" name="favorite" @click.stop.prevent="saveFavorited(item.channel_id)"><i class="fas fa-star"></i></button>
												<button class="share btn" @click.stop.prevent="share(item.video_id, item.thumbnail)" id="share" type="button" name="share"><i class="fas fa-share"></i>
											</button></div>
										</div>
									</div>
								</div>
							<div
								class="fa-left white-circle btn-prev d-n left-wow"
								data-element="wow"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-left"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
									></path>
								</svg>
							</div>
							<div
								class="fa-right white-circle btn-next right-wow"
								data-element="wow"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-right"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
									></path>
								</svg>
							</div>
						</div>
					</div>
				</template>
				<template id="watch-section">
					<section id="watch" class="watch">
						<h4>WATCH</h4>
						<div class="now-wrapper">
							<div class="wrapper">
								<div v-for="item in items" class="card" :style="{backgroundImage:`url(${item.channel_logo})`}">
									<div @click="selectWatch(item.video_id)">
										<p class="programChannel_num">{{ item.channel_no }}</p>
										<p class="programTitle">{{ item.channel_title }}</p>
									</div>
								</div>
							</div>
							<div
								class="fa-left white-circle btn-prev d-n left-watch"
								data-element="watch"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-left"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
									></path>
								</svg>
							</div>
							<div
								class="fa-right white-circle btn-next right-watch"
								data-element="watch"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-right"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
									></path>
								</svg>
							</div>
						</div>
					</section>
				</template>
				
				<template id="explore-section">
					<section id="explore" class="explore">
						<h4>EXPLORE</h4>
						<div class="now-wrapper">
							
							<div class="wrapper">
								<div class="card" v-for="item in categories" :style="{backgroundImage:`url('${baseUrl}/assets/images/category/${item.name}.jpg')`}" @click="selectCategory(item.id, item.name)">
									<div>
										<p class="category-name">{{ item.name }}</p>
										<p class="category-desc"></p>
									</div>
								</div>
							</div>
							<div
								class="fa-left white-circle btn-prev d-n left-explore"
								data-element="explore"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-left"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
									></path>
								</svg>
							</div>
							<div
								class="fa-right white-circle btn-next right-explore"
								data-element="explore"
							>
								<svg
									class="NavButton__icon___Pw1KL svg-right"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 192 320"
								>
									<path
										d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
									></path>
								</svg>
							</div>
						</div>
					</section>
				</template>
			</section>
			<template id="category-channel-section">
				<div class="category-channel pt-150" id="category-channel">
					<h3>
						<a href="javascript:void(0)" @click="backExplore">Explore </a> >
						<a href="javascript:void(0)" class="cat-name"  @click="showChannels()">{{category}}</a> 
						<a href="javascript:void(0)" class="channel-title" @click="showPlaylist()">{{channel}}</a>	
						<a href="javascript:void(0)" class="video-title" >{{playlist_title}}</a>	
					</h3>
					<div class="wrapper-channel" id="wrapper-channel">
						<div v-if="!haveRecords">No channels available ...</div>
						<div v-else class="card card-channel" v-for="item in items">
							<div class="thumbnail" @click="show(item.channel_id, item.playlist_id, item.title, item.type, item.video_id)">
								<img class="lazy" width="640" height="480" :src="item.thumbnail" loading="lazy">
							</div>
							<div class="content" style="">
								<h2>{{item.title}}</h2>	
								<div class="action">
									<button style="" class="favorite btn"  type="button" name="favorite"><i class="fas fa-star"></i></button>
									<button class="share btn" id="share" type="button" name="share"><i class="fas fa-share"></i></button>
								</div>
							</div>
						</div>
					</div>

					<div class="spinner">
						<i class="fas fa-circle-notch fa-spin"></i>
					</div>
					<button class="btn-load d-n" type="button">LOAD MORE</button>
				</div>
			</template>

			<!-- start read section -->
			<template id="read-section">
				<section id="read" class="read">
					<div class="read-wrapper">
						<h3>Read</h3>
						<div class="wrapper">
							<div class="category">
								<ul>
									<li v-for="(category, index) in categories" :class="{ active: index === indexId ? isActive : '' }">
										<a href="javascript:void(0)" @click="showArticle(category.id, index)" >{{category.name}}</a>
									</li>
								</ul>
							</div>
							<div id="show-read"></div>
							<div class="read-content">
								<div class="card" v-for="article in articles">
									<div class="image">
										<img loading="lazy" class="featured-img" :src="article.banner">
										<div class="source-content">
											<div class="source-logo">
												<img loading="lazy" class="source-logo" :src="article.thumbnail" alt="">
											</div>
										</div>
									</div>
									<div class="details">
										<h3 @click="readDetails(article.id)">{{article.title}}</h3>
										<span class="posted">{{article.date_created}}</span>
										
										<p class="description">{{article.blurb}}</p>
										<div class="source">
											<div class="action">
												<button class="favorite" @click.stop.prevent="saveFavorited(article.id)" name="favorite"  type="button">
													<i class="fas fa-star"></i>
												</button>
												<button class="share" @click.stop.prevent="share(article.id, article.thumbnail)" id="share" type="button" name="share">
													<i class="fas fa-share"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</template>
			<template id="read-details-section">
				<section class="read-promp-section d-n" id="read-promp-section">
					<div class="article">
						<div class="header">
							<h3 class="title"></h3>
							<a href="javascript:void(0)" class="page-link" @click="close">
								<span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</a>
						</div>
						<div class="article-body">
							<div class="banner">
								<img :src="banner" alt="" srcset="" class="img-banner" />
								<div class="source">
									<img :src="thumbnail" loading="lazy" class="img-source" />
								</div>
							</div>
							<div class="content">{{blurb}}</div>
							<div class="action">
								<a v-bind:href="url" target="_blank" class="target-action">
									<div class="icon">
										<svg
											width="19"
											height="13"
											viewBox="0 0 19 13"
											fill="none"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path
												d="M18 5L9.5 12L1 5"
												stroke="#009688"
												stroke-width="1.5"
											></path>
											<path
												d="M18 1L9.5 8L1 1"
												stroke="#009688"
												stroke-width="1.5"
											></path>
										</svg>
									</div>
									<button aria-label="Read Full Story">
										Read Full Story
									</button>
								</a>
							</div>
						</div>
					</div>
				</section>
			</template>
			<!-- end read section-->

			<template id="local-section">
				<section id="local" class="local">
					<h3>Local</h3>
					<div class="filter">
						<div><a id="all" href="javascript:void(0)" @click="selectLocation('all')">All</a></div>
						<div v-for="location in locations" @click="selectLocation(location.id)"><a id="11"> {{location.name}}</a></div>
					</div>
					<h4 class="local-label">All</h4>
					<div class="now-wrapper">
						<div class="related-videos">
							<div v-for="video in videos" class="card" :style="{backgroundImage:`url(${video.thumbnail})`}">
								<div class="card-body" @click="play(video.video_id)">
									<div class="items">
										<p>{{ video.title }}</p>
										<div class="action">
											<button class="favorite btn" @click.stop.prevent="save(video.id)" type="button" name="favorite" data-type="program" data-id="16252"><i class="fas fa-star"></i></button>
											<button class="share btn" @click.stop.prevent="share(video.video_id, video.thumbnail)" type="button" name="share"><i class="fas fa-share"></i></button> 
										</div>
									</div>
								</div>
							</div>
						</div>
						<div
							class="fa-left white-circle btn-prev d-n left-local"
							data-element="local"
						>
							<svg
								class="NavButton__icon___Pw1KL svg-left"
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 192 320"
							>
								<path
									d="M192 32.4L159.7 0 0 160l159.7 160 32.3-32.4L64.7 160z"
								></path>
							</svg>
						</div>
						<div
							class="fa-right white-circle btn-next right-local"
							data-element="local"
						>
							<svg
								class="NavButton__icon___Pw1KL svg-right"
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 192 320"
							>
								<path
									d="M0 32.4L32.3 0 192 160 32.3 320 0 287.6 127.3 160z"
								></path>
							</svg>
						</div>
					</div>
				</section>
			</template>
		</div>
		<footer>
			<div class="container-footer">
				<div class="social">
					<a href="https://www.facebook.com/nowcorpph" target="_blank"><i class="fab fa-facebook-square"></i></a>
					<a href="https://www.instagram.com/nowcorpph/" target="_blank"><i class="fab fa-instagram-square"></i></a>
					<a href="https://twitter.com/NOWCORPORATION" target="_blank"><i class="fab fa-twitter"></i></a>
					<a href="https://www.youtube.com/channel/UCy755VeDk6AnmEvSANmnlNw" target="_blank"><i class="fab fa-youtube"></i></a>
				</div>
				<div class="container">
					<div class="item">
						<p><a href="">About Us</a></p>
						<p><a href="">Contact Us</a></p>
						<p><a href="">Site Map</a></p>
						<p>
							<br />
							NOWNa &copy; <?= date('Y') ?>
						</p>
					</div>
					<div class="item">
						<p><a href="">Terms of Use</a></p>
						<p><a href="https://nownetwork.ph/privacy-policy-now-network-website/" target="_blank">Privacy Policy</a></p>
						<p><a href="">Cookie preferences</a></p>
					</div>
				</div>
			</div>
		</footer>
		<div id="mainPlayerDiv" class="mainPlayerDiv hideMainPlayerDiv">
			<!-- <iframe
				width="100%"
				height="100%"
				frameborder="0"
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen
				style="display: none"
			></iframe> -->
			<div id="mainPlayer" class="mainPlayer">Login</div>
			<div class="backButton">
				<button>
					<svg
						viewBox="0 0 24 24"
						preserveAspectRatio="xMidYMid meet"
						focusable="false"
						class="style-scope yt-icon"
						style="
							pointer-events: none;
							display: block;
							width: 100%;
							height: 100%;
							fill: #0ab39f;
							width: 25px;
						"
					>
						<g class="style-scope yt-icon">
							<path
								d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
								class="style-scope yt-icon"
							></path>
						</g>
					</svg>
				</button>
			</div>
		</div>

		<!-- modal login here -->

		<div class="profileDiv" id="profileDiv">
			<div class="form-area">
				<div class="wrapper">
					<div class="header"></div>
					<div class="close"><span>&times;</span></div>
					<div class="content">
						<div id="profile" style="display: none">
							<!-- <form class="profileForm" id="profileForm" name="profileForm" action="" enctype="multipart/form-data"> -->
							<div class="message"></div>
							<div class="input-wrapper">
								<div class="photo">
									<span class="text"></span>
									<span class="change">Change</span>
								</div>
							</div>
							<form
								class="profileForm"
								id="profileForm"
								name="profileForm"
								action=""
							>
								<div class="input-wrapper">
									<label for="first_name">First Name</label>
									<input
										type="text"
										name="first_name"
										id="first_name"
										v-model="editName.firstname"
										required
									/>
									<input
										type="hidden"
										name="id"
										id="id"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="last_name">Last Name</label>
									<input
										type="text"
										name="last_name"
										id="last_name"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="email">Email</label>
									<input
										type="email"
										name="email"
										id="email"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="password"
										>Current Password</label
									>
									<input
										type="password"
										name="password"
										id="password"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="new_password"
										>New Password</label
									>
									<input
										type="password"
										name="new_password"
										id="new_password"
										value=""
									/>
								</div>
								<div class="input-wrapper">
									<label for="confirm_password"
										>Confirm Password</label
									>
									<input
										type="password"
										name="confirm_password"
										id="confirm_password"
										value=""
									/>
								</div>
								<div class="input-wrapper button">
									<button
										class="profile-button"
										type="submit"
										id="profile_submit"
										name="submit"
									>
										Save
									</button>
									<!-- <button class="profile-button" type="button">Edit</button> -->
									<button class="profile-button" type="reset">
										Cancel
									</button>
								</div>
							</form>
							<form
								action=""
								style="display: none"
								id="profileImageForm"
								enctype="multipart/form-data"
							>
								<div class="input-wrapper">
									<!-- <label for="image">Photo</label> -->
									<input
										type="file"
										name="image"
										id="image"
										value=""
									/>
									<input
										type="hidden"
										name="id"
										id="id"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper button">
									<button
										class="profile-button"
										type="submit"
										id="profileImageSubmit"
										name="submit"
									>
										Upload
									</button>
									<!-- <button class="profile-button" type="button">Edit</button> -->
									<button
										class="profile-button"
										type="button"
										id="profileImageCancel"
									>
										Cancel
									</button>
								</div>
							</form>
						</div>
						<div id="recent" style="display: none"></div>
						<div id="favorite" style="display: none"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- share modal-->
		<div class="modal" id="modal">
			<div class="modal-content">
				<div class="modal-title">
					<h2>Share</h2>
					<span class="close"
						><svg
							viewBox="0 0 24 24"
							preserveAspectRatio="xMidYMid meet"
							focusable="false"
							class="style-scope yt-icon"
							style="
								pointer-events: none;
								display: block;
								width: 100%;
								height: 100%;
							"
						>
							<g class="style-scope yt-icon">
								<path
									d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
									class="style-scope yt-icon"
								></path>
							</g></svg
					></span>
				</div>
				<div class="modal-body">
					<div class="input-wrapper">
						<input type="text" name="share-url" class="share-url" />
						<button class="btn-copy">copy</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end share modal-->

		<!-- edit info modal-->
		<div class="modal" id="edit-info-modal">
			<div class="modal-content">
				<div class="modal-title">
					<h2 class="header-title">Upload</h2>
					<span class="close"
						><svg
							viewBox="0 0 24 24"
							preserveAspectRatio="xMidYMid meet"
							focusable="false"
							class="style-scope yt-icon"
							style="
								pointer-events: none;
								display: block;
								width: 100%;
								height: 100%;
							"
						>
							<g class="style-scope yt-icon">
								<path
									d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
									class="style-scope yt-icon"
								></path>
							</g></svg
					></span>
				</div>
				<div class="modal-body" id="edit-info">
					<!-- upload form -->
					<form action="" id="upload-frm" class="uploader d-n">
						<input type="hidden" id="file-data" />
						<input
							type="file"
							name="image"
							class="file-upload"
							id="file-upload"
							ref="file"
							@change="handleFileUpload()"
						/>
						<label
							for="file-upload"
							class="upload-file"
							id="file-drag"
						>
							<img
								id="file-image"
								:src="image"
								alt="Preview"
								class="hidden"
							/>
							<div id="start">
								<i
									class="fa fa-download"
									aria-hidden="true"
								></i>
								<div class="text">photo here</div>
								<!-- <div>- or -</div> -->
								<div id="notimage" class="hidden">
									Please select an image
								</div>
								<span
									id="file-upload-btn"
									class="btn btn-primary"
									>Select a file</span
								>
							</div>
							<div id="response" class="hidden">
								<div id="messages"></div>
								<progress
									class="progress"
									id="file-progress"
									value="0"
								>
									<span>0</span>%
								</progress>
							</div>
						</label>
						<button
							class="btn upload"
							id="set-profile"
							type="button"
							name="set-profile"
							@click="submitFile()"
							disabled
						>
							Set as profile
						</button>
						<button class="btn cancel" type="button" name="cancel">
							Cancel
						</button>
					</form>
					<form
						action=""
						id="change-name"
						class="change-name frm d-n"
						name="change-name"
						v-on:submit.prevent="updateName('update_name')"
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="first_name">First name</label>
							<input
								type="text"
								name="first_name"
								v-model="firstname"
								class="input"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="last_name">Last name</label>
							<input type="text" name="last_name" v-model="lastname" class="input" />
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-pass"
						class="change-pass frm d-n"
						name="change-pass"
						v-on:submit.prevent="updatePass('update_pass')"
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="current-password"
								>Current Password</label
							>
							<input
								type="password"
								name="current_password"
								class="input"
								v-model="password"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="new_password">New password</label>
							<input
								type="password"
								name="new-password"
								class="input"
								v-model="new_password"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="confirm-password"
								>Confirm password</label
							>
							<input
								type="password"
								name="confirm-password"
								class="input"
								v-model="confirm_password"
								required
							/>
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-email"
						class="change-email frm d-n"
						name="change-email"
						required
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="email">Email</label>
							<input
								type="email"
								name="email"
								class="input"
								required
							/>
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-phone"
						class="change-phone frm d-n"
						name="change-phone"
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="phone">Phone number</label>
							<input type="text" name="phone" class="input" />
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
				</div>
			</div>
		</div>
		<!-- end info modal-->


		<!-- share modal-->
		<template id="share-template-modal"> 
			<div class="modal" id="share-modal">
				<div class="modal-content">
					<div class="modal-title">
						<h2>Share</h2>
						<span class="close" @click="close"
							><svg
								viewBox="0 0 24 24"
								preserveAspectRatio="xMidYMid meet"
								focusable="false"
								class="style-scope yt-icon"
								style="
									pointer-events: none;
									display: block;
									width: 100%;
									height: 100%;
								"
							>
								<g class="style-scope yt-icon">
									<path
										d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
										class="style-scope yt-icon"
									></path>
								</g></svg
						></span>
					</div>
					<div class="modal-body">
						<div class="share-img">
							<img
								src="https://i1.ytimg.com/vi/zrc2UAMnr94/hqdefault.jpg"
								alt=""
							/>
						</div>
						<div class="share-wrapper">
							<input type="text" name="share-url" class="share-url" />
							<button class="btn-copy" @click="copy">copy</button>
						</div>
					</div>
				</div>
			</div>
		</template>
			<!-- end share modal-->