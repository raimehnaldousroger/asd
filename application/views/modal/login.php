<div id="loginDiv" class="loginDiv d-n">
	<div class="form-area" id="memberApp">
		<div class="wrapper">
			<h1 class="form-title" v-if="loginModalStatus =='1'">Login</h1>
			<h1 class="form-title" v-if="registerModalStatus =='1'">Register</h1>
			<!-- login-->
			<form class="loginForm form-wrapper" v-if="loginModalStatus == true">
				<div class="input-wrapper">
					<label for="">Username</label>
					<input name="username" type="text" id="username" v-model="login.username">
					<label v-model="message.username" style="color: red">{{message.username}}</label>
				</div>
				<div class="input-wrapper">
					<label for="">Password</label>
					<input type="password" name="password" v-model="login.password">
					<label v-model="message.password" style="color: red">{{message.password}}</label>
				</div>
				<div class="input-wrapper">
					<button class="login-button" type="button" @click="loginBtn"> Login </button>
				</div>
				<div class="input-wrapper">
					<div class="text-center">
						<span>Not yet a member?</span>
						<a id="register-link" @click="changeModalStatus()">Register now</a>
					</div>
				</div>
			</form>

			<!-- register-->
			<form class="register form-wrapper" id="register-frm" v-if="registerModalStatus == true">
				<div class="input-wrapper">
					<label for="">Firstname</label>
					<input type="text" name="firstname" v-model="register.firstname">
					<label v-model="message.firstname" style="color: red">{{message.firstname}}</label>
				</div>
				<div class="input-wrapper">
					<label for="">Lastname</label>
					<input type="text" name="lastname" v-model="register.lastname">
					<label v-model="message.lastname" style="color: red">{{message.lastname}}</label>
				</div>
				<div class="input-wrapper">
					<label for="">Email</label>
					<input type="text" name="email" v-model="register.email">
					<label v-model="message.email" style="color: red">{{message.email}}</label>
				</div>
				<hr>
				<div class="input-wrapper">
					<label for="">Username</label>
					<input type="text" name="username" v-model="register.username">
					<label v-model="message.username" style="color: red">{{message.username}}</label>
				</div>
				<div class="input-wrapper">
					<label for="">Password</label>
					<input type="password" name="password" v-model="register.password">
					<label v-model="message.password" style="color: red">{{message.password}}</label>
				</div>
				<div class="input-wrapper term">
					
					<label v-model="message.consent" style="color: red">{{message.consent}}</label>
					<input type="checkbox" name="terms" id="terms" v-model="register.consent" />
					<label for="terms" class="text-left">I understand and agree to the
						<a href="#" class="terms-link" id="termsAndCondition">Terms and Conditions of Use</a>.</label
					>
				</div>
				<div class="input-wrapper">
					<input class="register-btn" type="button" name="submit" value="Register" @click="registerBtn()">
				</div>

				<div class="input-wrapper">
					<div class="text-center">
						<span>Already a member?</span>
						<a id="login-link" @click="changeModalStatus()">Login here.</a>
					</div>
				</div>
			</form>

			<!-- OTP -->
			<form class="register form-wrapper" id="register-frm" v-if="otpModalStatus == true">
				<div class="input-wrapper">
					<label for="">OTP</label>
					<input type="text" name="code" v-model="otp.code">
					<label v-model="message.otp" style="color: red">{{message.otp}}</label>
				</div>
				<div class="input-wrapper">
					<input class="otp-btn" type="button" name="submit" value="Activate your account" @click="otpBtn()">
				</div>
			</form>
		</div>
	</div>
</div>