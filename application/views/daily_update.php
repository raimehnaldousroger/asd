<?php
	if(isset($_GET['accept-cookies'])) {
		setcookie('accept-cookies', 'true', time() + 630427, '/');
		header('Location: ./');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link
			rel="icon"
			type="image/png"
			href="/v1/iptv_cms/images/logo_1.png"
		/>
		<title>NOWNa.com | Home</title>

		<base href="/v1/" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/ css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"/>
		<link
			rel="stylesheet"
			href="./public/fontawesome-free-5.14.0-web/css/all.min.css"
		/>
		<script src="./public/fontawesome-free-5.14.0-web/js/all.js"></script>
		<link rel="stylesheet" href="./public/styles/home.css" />
		<link rel="stylesheet" href="./public/styles/duDialog.min.css" />

		<!-- vue js, lodash jquery shortcode scripting, axios, toastr -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
		<!-- <script src="https://unpkg.com/vue@next"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"
		></script>
		<!-- toastr -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
		<!-- end -->

		<!-- Player Source -->
		<!-- <script src="https://cdn.jwplayer.com/libraries/51q7qFMr.js"></script> -->
		<!-- <script src="https://cdn.jwplayer.com/libraries/nl9Ce6gk.js"></script> -->
		<!-- <script src="http://content.jwplatform.com/libraries/NxsmWX2o.js"></script> -->
		<!-- <script type="text/javascript" src="http://p.jwpcdn.com/6/10/jwplayer.js"></script> -->
		<!-- <script src="./public/js/jwpsrv7.js"></script> -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script
			async
			src="https://www.googletagmanager.com/gtag/js?id=G-SWFDN8QWGP"
		></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag() {
				dataLayer.push(arguments);
			}
			gtag("js", new Date());

			gtag("config", "G-SWFDN8QWGP");
		</script>


		<style type="text/css">
			.cookie-banner {
	        	background: #ffffffbf;
			    z-index: -51;
			    padding: 20px 15px;
			    color: #000;
			    position: fixed;
			    z-index: 1000;
			    width: 100%;
			    bottom: 0px;
			}
				.cookie-banner .close {
					color: #000!important;
				}
			#app ul {
			  list-style-type: none;
			}
			#app li {
			  display: inline-block;
			}
			#app p {
				text-align: center;
				color: #000;
			}
			#app .news label {
				color: #000;
			}
			#app .videoCat input[type="checkbox"] {
			  	display: none;
			}
			#app .videoCat label {
				border: 1px solid #fff;
				padding: 10px;
				display: block;
				position: relative;
				margin: 10px;
				cursor: pointer;
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			#app .videoCat label::before {
				background-color: white;
				color: white;
				content: " ";
				display: block;
				border-radius: 50%;
				border: 1px solid grey;
				position: absolute;
				top: 5px;
				left: 5px;
				width: 25px;
				height: 25px;
				text-align: center;
				line-height: 28px;
				transition-duration: 0.4s;
				transform: scale(0);
			}
			#app .videoCat label img {
			  	height: 100px;
			  	width: 100px;
			  	transition-duration: 0.2s;
			  	transform-origin: 50% 50%;
			}

			#app .videoCat :checked+label {
		  		border-color: #ddd;
			}

			#app .videoCat :checked+label::before {
				content: "✓";
				background-color: #8ec300;
				transform: scale(1);
		        z-index: 990;
			}

			#app .videoCat :checked+label img {
				transform: scale(0.9);
				box-shadow: 0 0 5px #333;
				z-index: -1;
			}
		</style>
	</head>
	<body>
		<header>
			<nav>
				<a href="#live" class="logo page-link">
					<img
						src="./public/images/logo/NOW NA LOGO_White.png"
						alt="NOWNa"
					/>
				</a>
				<ul class="nav-links">
					<li><a href="#trending" class="page-link">Trending</a></li>
					<li><a href="#watch" class="page-link">Watch</a></li>
					<li><a href="#explore" class="page-link">Explore</a></li>
					<li><a href="#read" class="page-link">Read</a></li>
					<li><a href="#local" class="page-link">Local</a></li>
					<li class="log-mobile d-n">
						<a href="javascript:void(0)" onclick="logout()"
							>Logout</a
						>
					</li>
				</ul>
				<div class="search-wrapper-mobile d-n">
					<button
						type="submit"
						class="icon-search"
						name="search"
						data-click="1"
					>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="18"
							height="18"
							style="fill: #ffffff"
							class="search-svg"
						>
							<path
								d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z"
							></path>
						</svg>
						<span class="close d-n"
							><svg
								viewBox="0 0 24 24"
								preserveAspectRatio="xMidYMid meet"
								focusable="false"
								class="style-scope yt-icon"
								style="
									pointer-events: none;
									display: block;
									width: 100%;
									height: 100%;
									fill: #0ab39f;
									width: 25px;
								"
							>
								<g class="style-scope yt-icon">
									<path
										d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
										class="style-scope yt-icon"
									></path>
								</g></svg
						></span>
					</button>
				</div>
				<div
					class="profileLinks-mobile d-n profileLinks"
					id="profileLinks"
				></div>
				<ul class="login">
					<div class="search-wrapper search-wrapper-desktop">
						<button
							type="submit"
							class="icon-search"
							name="search"
							data-click="1"
						>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="18"
								height="18"
								style="fill: #ffffff"
								class="search-svg"
							>
								<path
									d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z"
								></path>
							</svg>
							<span class="close d-n"
								><svg
									viewBox="0 0 24 24"
									preserveAspectRatio="xMidYMid meet"
									focusable="false"
									class="style-scope yt-icon"
									style="
										pointer-events: none;
										display: block;
										width: 100%;
										height: 100%;
										fill: #0ab39f;
										width: 25px;
									"
								>
									<g class="style-scope yt-icon">
										<path
											d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
											class="style-scope yt-icon"
										></path>
									</g></svg
							></span>
						</button>
					</div>
					<li
						id="profileLinks"
						class="profileLinks-d profileLinks"
					></li>
					<li id="loginLinks">
						<a href="./index.html">Login</a> |
						<a href="#">Register</a>
					</li>
				</ul>
				<div class="burger">
					<div class="line1"></div>
					<div class="line2"></div>
					<div class="line3"></div>
				</div>
			</nav>
		</header>
		<div class="container">
			<?php if(!isset($_COOKIE['accept-cookies'])) { ?>
				<div class="cookie-banner">
					<p>The NOW Corp and NOWaccount web sites may use “cookies” to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. <a href="?accept-cookies" class="close">Accept</a> </p>
				</div>
			<?php } ?>
			<!-- Live ====================================s-->
			<section class="live-section" id="live-section">
				<div id="live"></div>
			</section>

			<!-- start profile ================================ -->
			<section class="profile-container d-n">
				<div class="profile-wrapper">
					<div class="title">
						<span class="logged"></span>
						<h2>Personal info</h2>
						<span class="close" style="width: 25px"
							><svg
								viewBox="0 0 24 24"
								preserveAspectRatio="xMidYMid meet"
								focusable="false"
								class="style-scope yt-icon"
								style="
									pointer-events: none;
									display: block;
									width: 100%;
									height: 100%;
								"
							>
								<g class="style-scope yt-icon">
									<path
										d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
										class="style-scope yt-icon"
									></path>
								</g></svg
						></span>
					</div>

					<div class="card">
						<div class="card-title"><h2>Basic info</h2></div>
						<input type="hidden" name="prof-id" id="prof-id" />
						<div class="divider" element="photo">
							<div class="text text-mobile">
								<h3 class="divider-name">Photo</h3>
							</div>
							<div class="text-desc avatar-info">Add a photo</div>
							<div class="photo-wrapper">
								<div class="photo avatar">
									<h2 class="avatar-name">J</h2>
									<div class="photo-icon">
										<i class="fas fa-camera"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="border-top"></div>
						<div class="divider name" element="name">
							<div class="text text-mobile">
								<h3 class="divider-name">Name</h3>
							</div>
							<div class="text name-info">JUNREL DEVOCION</div>
							<div class="text edit">
								<svg
									aria-hidden="true"
									focusable="false"
									data-prefix="fal"
									data-icon="pencil"
									role="img"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 512"
									class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
								>
									<path
										fill="currentColor"
										d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
										class=""
									></path>
								</svg>
							</div>
						</div>
						<div class="border-top"></div>
						<div class="divider" element="password">
							<div class="text text-mobile">
								<h3 class="divider-name">Password</h3>
							</div>
							<div class="pass password-info">••••••••</div>
							<div class="text edit">
								<svg
									aria-hidden="true"
									focusable="false"
									data-prefix="fal"
									data-icon="pencil"
									role="img"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 512"
									class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
								>
									<path
										fill="currentColor"
										d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
										class=""
									></path>
								</svg>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-title"><h2>Contact info</h2></div>

						<div class="divider email" element="email">
							<div class="text text-mobile">
								<h3 class="divider-name">Email</h3>
							</div>
							<div class="text email-info">
								junrel.devocion@ictv.ph
							</div>
							<div class="text edit">
								<svg
									aria-hidden="true"
									focusable="false"
									data-prefix="fal"
									data-icon="pencil"
									role="img"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 512"
									class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
								>
									<path
										fill="currentColor"
										d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
										class=""
									></path>
								</svg>
							</div>
						</div>
						<div class="border-top"></div>
						<div class="divider phone" element="mobile">
							<div class="text text-mobile">
								<h3 class="divider-name">Phone</h3>
							</div>
							<div class="text-desc contact-info">0912345678</div>
							<div class="text edit">
								<svg
									aria-hidden="true"
									focusable="false"
									data-prefix="fal"
									data-icon="pencil"
									role="img"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 512 512"
									class="svg-inline--fa fa-pencil fa-w-16 fa-fw fa-2x"
								>
									<path
										fill="currentColor"
										d="M493.255 56.236l-37.49-37.49c-24.993-24.993-65.515-24.994-90.51 0L12.838 371.162.151 485.346c-1.698 15.286 11.22 28.203 26.504 26.504l114.184-12.687 352.417-352.417c24.992-24.994 24.992-65.517-.001-90.51zm-95.196 140.45L174 420.745V386h-48v-48H91.255l224.059-224.059 82.745 82.745zM126.147 468.598l-58.995 6.555-30.305-30.305 6.555-58.995L63.255 366H98v48h48v34.745l-19.853 19.853zm344.48-344.48l-49.941 49.941-82.745-82.745 49.941-49.941c12.505-12.505 32.748-12.507 45.255 0l37.49 37.49c12.506 12.506 12.507 32.747 0 45.255z"
										class=""
									></path>
								</svg>
							</div>
						</div>
						<!-- vuejs retrieve categories to be choose and save user's preffered video - RAI -->
					<div class="card">
						<div class="card-title"><h2>My Preference</h2></div>
						<style type="text/css">
							/*#app{
    							color: #000;
							}*/
							#app h1 {
								color: #000;
							}
							#app ul {
							  list-style-type: none;
							}

							#app li {
							  display: inline-block;
							}

							#app p {
								text-align: center;
								color: #000;
							}

							#app .news label {
								color: #000;
							}


								#app .videoCat input[type="checkbox"] {
								  	display: none;
								}
								#app .videoCat label {
									border: 1px solid #fff;
									padding: 10px;
									display: block;
									position: relative;
									margin: 10px;
									cursor: pointer;
									-webkit-touch-callout: none;
									-webkit-user-select: none;
									-khtml-user-select: none;
									-moz-user-select: none;
									-ms-user-select: none;
									user-select: none;
								}
								#app .videoCat label::before {
									background-color: white;
									color: white;
									content: " ";
									display: block;
									border-radius: 50%;
									border: 1px solid grey;
									position: absolute;
									top: 5px;
									left: 5px;
									width: 25px;
									height: 25px;
									text-align: center;
									line-height: 28px;
									transition-duration: 0.4s;
									transform: scale(0);
								}
								#app .videoCat label img {
								  	height: 100px;
								  	width: 100px;
								  	transition-duration: 0.2s;
								  	transform-origin: 50% 50%;
								}

							#app .videoCat :checked+label {
						  		border-color: #ddd;
							}

							#app .videoCat :checked+label::before {
								content: "✓";
								background-color: #8ec300;
								transform: scale(1);
						        z-index: 990;
							}

							#app .videoCat :checked+label img {
								transform: scale(0.9);
								box-shadow: 0 0 5px #333;
								z-index: -1;
							}
						</style>

						<div id="app">
							<div class="videoCat">
								<h1>Video Category</h1>
								<ul>
									<li v-for="exploreList in exploreList">
										<input type="checkbox" :id="'cb'+exploreList.id" v-model="PrefCategoryID" :name="exploreList.name" :value="exploreList.id" @change="categorySelected($event)" >
										<label :for="'cb'+exploreList.id"><img :src="'./public/images/category/'+exploreList.name+'.jpg'"></label>
										<p>{{exploreList.name}}</p>
									</li>
								</ul>
							</div>
							<div class="news">
								<h1>News Category</h1>
								<div v-for="newsCatList in newsCatList">
									<input type="checkbox" :id="'news'+newsCatList.id" v-model="PrefNewsCategoryID" :name="newsCatList.name" :value="newsCatList.id" @change="categorySelected($event)" >
									<label :for="'news'+newsCatList.id">{{newsCatList.name}}</label>
								</div>
							</div>
							<!-- <span>{{PrefNewsCategoryID}}</span> -->
						</div>
						
					</div>
					<!-- end -->
					</div>
				</div>
			</section>
			<!-- end profile ================================-->

			<!-- recently watched ================================ -->
			<section class="recently-section pt-150 d-n" id="recently-section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Recently watched</h4>
								<span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</div>
							<button type="button" class="clear-history">
								<i class="fas fa-history"></i> Clear
							</button>
							<div class="video-list"></div>
						</div>
					</div>
				</div>
			</section>
			<!--End recently watch ================================ -->

			<!-- recently read ================================ -->
			<section
				class="recently-read-section pt-150 d-n"
				id="recently-read-section"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Recently read</h4>
								<span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</div>
							<button type="button" class="clear-history">
								<i class="fas fa-history"></i> Clear
							</button>
							<div class="video-list"></div>
						</div>
					</div>
				</div>
			</section>

			<!--End recently read ================================ -->

			<!-- Favorite ================================ -->
			<section
				class="favorite-section fav-section pt-150 d-n"
				id="favorite-section"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Favorited</h4>

								<span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span>
							</div>

							<div class="video-list"></div>
						</div>
					</div>
				</div>
			</section>
			<!--End Favorite ================================ -->

			<!-- Favorite ================================ -->
			<section
				class="favorite-section-article fav-section d-n"
				id="favorite-section-article"
			>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="title">
								<h4>Read</h4>
								<!-- <span class="close"
									><svg
										viewBox="0 0 24 24"
										preserveAspectRatio="xMidYMid meet"
										focusable="false"
										class="style-scope yt-icon"
										style="
											pointer-events: none;
											display: block;
											width: 100%;
											height: 100%;
										"
									>
										<g class="style-scope yt-icon">
											<path
												d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
												class="style-scope yt-icon"
											></path>
										</g></svg
								></span> -->
							</div>

							<div class="list-article"></div>
						</div>
					</div>
				</div>
			</section>
			<!--End Favorite ================================ -->

			<!--Start Search ================================ -->
			<div class="search-container d-n" id="search">
				<div class="search-wrapper">
					<div class="search-input">
						<input
							type="text"
							name="search"
							id="search"
							autofocus
							placeholder="Search...."
						/>
						<button type="submit" class="btn-search" name="search">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="18"
								height="18"
							>
								<path
									fill="rgb(151,151,159)"
									d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z"
								></path>
							</svg>
						</button>
					</div>
					<div class="results-container">
						<div class="results-wrapper">
							<div class="results"></div>
							<div class="spinner">
								<i class="fas fa-circle-notch fa-spin"></i>
							</div>
							<button class="btn-load d-n">LOAD MORE</button>
						</div>
					</div>
				</div>
				<div class="results-read-wrapper d-n">
					<div class="wrapper">
						<h3 class="title-result">Read</h3>
						<div class="results"></div>
						<div class="full-read">
							<button class="back">
								<i class="fa fa-times"></i>
							</button>
							<h3 class="title"></h3>
							<div class="img-banner"></div>
							<p class="content"></p>
							<div class="btn-group">
								<svg
									width="19"
									height="13"
									viewBox="0 0 19 13"
									fill="none"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										d="M18 5L9.5 12L1 5"
										stroke="#009688"
										stroke-width="1.5"
									></path>
									<path
										d="M18 1L9.5 8L1 1"
										stroke="#009688"
										stroke-width="1.5"
									></path>
								</svg>
								<button class="full-story">
									Read full story
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end Search ================================ -->

			
		</div>

		<div class="container">
			<style type="text/css">
				.header {
				    position: sticky!important;
				}
				.now-wrapper {
				    display: inline-block!important;
			        width: 30%;
				}
				.dailyUpdateTrending {
				    width: 80%;
    				margin: 0 auto;
				}
				.container .top {
					top: 100px!important;
					height: 100vh;
				}
			</style>
			<?php if(!isset($_COOKIE['accept-cookies'])) { ?>
				<div class="cookie-banner">
					<p>The NOW Corp and NOWaccount web sites may use “cookies” to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. <a href="?accept-cookies" class="close">Accept</a> </p>
				</div>
			<?php } ?>
			<div class="top">
				<div id="trending" class="trending">
					<template id="appEmailTemp">
						<div class="dailyUpdateTrending">
							<h3 id="greetingsUser"></h3>
							<p>The NOW Corp and NOWaccount web sites may use “cookies” to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>
							<h1>LATEST VIDEOS</h1>
							<div class="now-wrapper">
								<h4>NOW</h4>
								<div class="nowCat" v-for="now in newHotList">
									<div v-for="nowPost in now.channel_id" v-if="nowPost.tag_id === '1'">
										<img :src="'https://i1.ytimg.com/vi/'+nowPost.thumbnail">
										<p>{{nowPost.title}} </p>
									</div>
								</div>
							</div>
							<div class="now-wrapper">
								<h4>HOT</h4>
								<div class="hotCat" v-for="now in newHotList">
									<div v-for="nowPost in now.channel_id" v-if="nowPost.tag_id === '2'">
										<img :src="'https://i1.ytimg.com/vi/'+nowPost.thumbnail">
										<p>{{nowPost.title}} </p>
									</div>
								</div>
							</div>
							<div class="now-wrapper">
								<h4>WOW</h4>
								<div class="wowCat" v-for="now in newHotList">
									<div v-for="nowPost in now.channel_id" v-if="nowPost.tag_id === '3'">
										<img :src="'https://i1.ytimg.com/vi/'+nowPost.thumbnail">
										<p>{{nowPost.title}} </p>
									</div>
								</div>
							</div>

							<h1>LATEST NEWS</h1>
							<div class="now-wrapper">
								<div class="nowCat" v-for="now in newHotList">
									<div v-for="nowPost in now.news_id">
										<div v-for="asd in nowPost">
											<img :src="asd.banner">
											<p>{{asd.title}} </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</template>
				</div>
			</div>
		</div>

		<footer>
			<div class="container-footer">
				<div class="social">
					<a href=""><i class="fab fa-facebook-square"></i></a>
					<a href=""><i class="fab fa-instagram-square"></i></a>
					<a href=""><i class="fab fa-twitter"></i></a>
					<a href=""><i class="fab fa-youtube"></i></a>
				</div>
				<div class="container">
					<div class="item">
						<p><a href="">About Us</a></p>
						<p><a href="">Contact Us</a></p>
						<p><a href="">Site Map</a></p>
						<p>
							<br />
							NOWNa &copy; 2020
						</p>
					</div>
					<div class="item">
						<p><a href="">Terms of Use</a></p>
						<p><a href="">Privacy Policy</a></p>
						<p><a href="">Cookie preferences</a></p>
					</div>
				</div>
			</div>
		</footer>
		<div id="mainPlayerDiv" class="mainPlayerDiv hideMainPlayerDiv">
			<!-- <iframe
				width="100%"
				height="100%"
				frameborder="0"
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen
				style="display: none"
			></iframe> -->
			<div id="mainPlayer" class="mainPlayer">Login</div>
			<div class="backButton">
				<button>
					<svg
						viewBox="0 0 24 24"
						preserveAspectRatio="xMidYMid meet"
						focusable="false"
						class="style-scope yt-icon"
						style="
							pointer-events: none;
							display: block;
							width: 100%;
							height: 100%;
							fill: #0ab39f;
							width: 25px;
						"
					>
						<g class="style-scope yt-icon">
							<path
								d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
								class="style-scope yt-icon"
							></path>
						</g>
					</svg>
				</button>
			</div>
		</div>

		<div id="loginDiv" class="loginDiv loginDivShow">
			<div class="form-area">
				<div class="wrapper">
					<div class="alert d-n">
						<h3 class="alert-title"></h3>
						<a target="_blank" href="https://mail.google.com/"
							>gmail.com</a
						>
					</div>
					<h1 class="form-title">Login</h1>
					<!-- login-->
					<form
						method="POST"
						id="loginForm"
						class="loginForm form-wrapper"
					>
						<div class="input-wrapper">
							<label for="">Email</label>
							<input
								type="email"
								name="email"
								id="email"
								autocomplete="off"
								required
								maxlength="150"
							/>
						</div>
						<div class="input-wrapper">
							<label for="">Password</label>
							<input
								type="password"
								name="password"
								autocomplete="off"
								id="password"
								required
								maxlength="50"
							/>
						</div>
						<div class="input-wrapper">
							<button class="login-button" type="submit">
								Login
							</button>
						</div>
						<div class="input-wrapper">
							<div class="text-center">
								<span>Not yet a member?</span>
								<a href="javascript:void(0)" id="register-link"
									>Register now</a
								>
							</div>
						</div>
					</form>

					<!-- register-->
					<form
						method="POST"
						class="register form-wrapper d-n"
						id="register-frm"
					>
						<div class="input-wrapper">
							<label for="">First Name</label>
							<input
								type="text"
								name="firstname"
								maxlength="30"
							/>
						</div>
						<div class="input-wrapper">
							<label for="">Last Name</label>
							<input type="text" name="lastname" maxlength="30" />
						</div>
						<div class="input-wrapper">
							<label for="">Email</label>
							<input type="email" name="email" maxlength="50" />
						</div>
						<div class="input-wrapper">
							<label for="">Password</label>
							<input
								type="password"
								name="password"
								maxlength="20"
							/>
						</div>
						<div class="input-wrapper term">
							<input type="checkbox" name="terms" id="terms" />
							<label class="text-left"
								>I understand and agree to the
								<a
									href="#"
									class="terms-link"
									id="termsAndCondition"
									>Terms and Conditions of Use</a
								>.</label
							>
						</div>
						<div class="input-wrapper">
							<input
								class="register-btn"
								type="submit"
								name="submit"
								value="Register"
							/>
						</div>

						<div class="input-wrapper">
							<div class="text-center">
								<span>Already a member?</span>
								<a href="javascript:void(0)" id="login-link"
									>Login here.</a
								>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="profileDiv" id="profileDiv">
			<div class="form-area">
				<div class="wrapper">
					<div class="header"></div>
					<div class="close"><span>&times;</span></div>
					<div class="content">
						<div id="profile" style="display: none">
							<!-- <form class="profileForm" id="profileForm" name="profileForm" action="" enctype="multipart/form-data"> -->
							<div class="message"></div>
							<div class="input-wrapper">
								<div class="photo">
									<span class="text"></span>
									<span class="change">Change</span>
								</div>
							</div>
							<form
								class="profileForm"
								id="profileForm"
								name="profileForm"
								action=""
							>
								<div class="input-wrapper">
									<label for="first_name">First Name</label>
									<input
										type="text"
										name="first_name"
										id="first_name"
										value=""
										required
									/>
									<input
										type="hidden"
										name="id"
										id="id"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="last_name">Last Name</label>
									<input
										type="text"
										name="last_name"
										id="last_name"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="email">Email</label>
									<input
										type="email"
										name="email"
										id="email"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="password"
										>Current Password</label
									>
									<input
										type="password"
										name="password"
										id="password"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper">
									<label for="new_password"
										>New Password</label
									>
									<input
										type="password"
										name="new_password"
										id="new_password"
										value=""
									/>
								</div>
								<div class="input-wrapper">
									<label for="confirm_password"
										>Confirm Password</label
									>
									<input
										type="password"
										name="confirm_password"
										id="confirm_password"
										value=""
									/>
								</div>
								<div class="input-wrapper button">
									<button
										class="profile-button"
										type="submit"
										id="profile_submit"
										name="submit"
									>
										Save
									</button>
									<!-- <button class="profile-button" type="button">Edit</button> -->
									<button class="profile-button" type="reset">
										Cancel
									</button>
								</div>
							</form>
							<form
								action=""
								style="display: none"
								id="profileImageForm"
								enctype="multipart/form-data"
							>
								<div class="input-wrapper">
									<!-- <label for="image">Photo</label> -->
									<input
										type="file"
										name="image"
										id="image"
										value=""
									/>
									<input
										type="hidden"
										name="id"
										id="id"
										value=""
										required
									/>
								</div>
								<div class="input-wrapper button">
									<button
										class="profile-button"
										type="submit"
										id="profileImageSubmit"
										name="submit"
									>
										Upload
									</button>
									<!-- <button class="profile-button" type="button">Edit</button> -->
									<button
										class="profile-button"
										type="button"
										id="profileImageCancel"
									>
										Cancel
									</button>
								</div>
							</form>
						</div>
						<div id="recent" style="display: none"></div>
						<div id="favorite" style="display: none"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- share modal-->
		<div class="modal" id="modal">
			<div class="modal-content">
				<div class="modal-title">
					<h2>Share</h2>
					<span class="close"
						><svg
							viewBox="0 0 24 24"
							preserveAspectRatio="xMidYMid meet"
							focusable="false"
							class="style-scope yt-icon"
							style="
								pointer-events: none;
								display: block;
								width: 100%;
								height: 100%;
							"
						>
							<g class="style-scope yt-icon">
								<path
									d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
									class="style-scope yt-icon"
								></path>
							</g></svg
					></span>
				</div>
				<div class="modal-body">
					<div class="input-wrapper">
						<input type="text" name="share-url" class="share-url" />
						<button class="btn-copy">copy</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end share modal-->

		<!-- edit info modal-->
		<div class="modal" id="edit-info-modal">
			<div class="modal-content">
				<div class="modal-title">
					<h2 class="header-title">Upload</h2>
					<span class="close"
						><svg
							viewBox="0 0 24 24"
							preserveAspectRatio="xMidYMid meet"
							focusable="false"
							class="style-scope yt-icon"
							style="
								pointer-events: none;
								display: block;
								width: 100%;
								height: 100%;
							"
						>
							<g class="style-scope yt-icon">
								<path
									d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
									class="style-scope yt-icon"
								></path>
							</g></svg
					></span>
				</div>
				<div class="modal-body">
					<!-- upload form -->
					<form action="" id="upload-frm" class="uploader d-n">
						<input type="hidden" id="file-data" />
						<input
							type="file"
							name="image"
							class="file-upload"
							id="file-upload"
						/>
						<label
							for="file-upload"
							class="upload-file"
							id="file-drag"
						>
							<img
								id="file-image"
								src=""
								alt="Preview"
								class="hidden"
							/>
							<div id="start">
								<i
									class="fa fa-download"
									aria-hidden="true"
								></i>
								<div class="text">photo here</div>
								<!-- <div>- or -</div> -->
								<div id="notimage" class="hidden">
									Please select an image
								</div>
								<span
									id="file-upload-btn"
									class="btn btn-primary"
									>Select a file</span
								>
							</div>
							<div id="response" class="hidden">
								<div id="messages"></div>
								<progress
									class="progress"
									id="file-progress"
									value="0"
								>
									<span>0</span>%
								</progress>
							</div>
						</label>
						<button
							class="btn upload"
							id="set-profile"
							type="button"
							name="set-profile"
							disabled
						>
							Set as profile
						</button>
						<button class="btn cancel" type="button" name="cancel">
							Cancel
						</button>
					</form>
					<form
						action=""
						id="change-name"
						class="change-name frm d-n"
						name="change-name"
						required
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="first_name">First name</label>
							<input
								type="text"
								name="first_name"
								class="input"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="last_name">Last name</label>
							<input type="text" name="last_name" class="input" />
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-pass"
						class="change-pass frm d-n"
						name="change-pass"
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="current-password"
								>Current Password</label
							>
							<input
								type="password"
								name="current_password"
								class="input"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="new_password">New password</label>
							<input
								type="password"
								name="new_password"
								class="input"
								required
							/>
						</div>
						<div class="input-wrapper">
							<label for="confirm-password"
								>Confirm password</label
							>
							<input
								type="password"
								name="confirm_password"
								class="input"
								required
							/>
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-email"
						class="change-email frm d-n"
						name="change-email"
						required
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="email">Email</label>
							<input
								type="email"
								name="email"
								class="input"
								required
							/>
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
					<form
						action=""
						id="change-phone"
						class="change-phone frm d-n"
						name="change-phone"
					>
						<input type="hidden" name="id" />
						<div class="input-wrapper">
							<label for="phone">Phone number</label>
							<input type="text" name="phone" class="input" />
						</div>
						<button class="btn save" type="submit">Save</button>
						<button class="btn cancel" type="button">Cancel</button>
					</form>
				</div>
			</div>
		</div>
		<!-- end share modal-->

		<!-- share modal-->
		<div class="modal" id="share-modal">
			<div class="modal-content">
				<div class="modal-title">
					<h2>Share</h2>
					<span class="close"
						><svg
							viewBox="0 0 24 24"
							preserveAspectRatio="xMidYMid meet"
							focusable="false"
							class="style-scope yt-icon"
							style="
								pointer-events: none;
								display: block;
								width: 100%;
								height: 100%;
							"
						>
							<g class="style-scope yt-icon">
								<path
									d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
									class="style-scope yt-icon"
								></path>
							</g></svg
					></span>
				</div>
				<div class="modal-body">
					<div class="share-img">
						<img
							src="https://i1.ytimg.com/vi/zrc2UAMnr94/hqdefault.jpg"
							alt=""
						/>
					</div>
					<div class="share-wrapper">
						<input type="text" name="share-url" class="share-url" />
						<button class="btn-copy">copy</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end share modal-->

		<script src="./public/js/jquery-3.5.1.min.js"></script>

		<script type="text/javascript">
			$(function () {
			    var appEmailTemp = new Vue({
			        el:'#appEmailTemp',
			        data:{
			           newHotList: [{}],
			        },
			        mounted() {
						let first_name = getLoggedIn("user-first-name");
						$('#greetingsUser').text('Hi, '+ first_name +',');

						this.setList();
			        },
			        methods: { 
			            setList: async function () {
			                try {
			                    const response = await axios.get('/v1/iptv_api/emailPreference/displayEmail');
			                    this.newHotList = response.data.data;
			                } catch (error) {
			                    console.log('Error Connecting to server');
			                }
			            },
			        },
			    });
			});
		</script>

		<script src="./public/js/jquery.validate.min.js"></script>
		<script src="./public/js/jquery.lazy.min.js"></script>
		<script src="./public/js/duDialog.min.js"></script>

		<script src="./public/js/app.js"></script>
		<script src="./public/js/player.js"></script>
		<script src="./public/js/form.js"></script>
		<script src="./public/js/login2.js"></script>

		<script src="./public/js/homepage/profile.js"></script>
		<script src="./public/js/homepage/profile2.js"></script>
		<script src="./public/js/homepage/loginChecker.js"></script>
		<script src="./public/js/homepage/lastClick.js"></script>

		<script src="./public/js/homepage/trending.js"></script>
		<script src="./public/js/homepage/watch.js"></script>
		<script src="./public/js/homepage/explore.js"></script>
		<script src="./public/js/homepage/read.js"></script>
		<script src="./public/js/homepage/local.js"></script>
		<script src="./public/js/homepage/search.js"></script>
		<script src="./public/js/homepage/livestream.js"></script>
		<script src="./public/js/homepage/recentlyWatch.js"></script>
		<script src="./public/js/homepage/favorited.js"></script>
		<script src="./public/js/homepage/share.js"></script>
		<script src="./public/js/homepage/recentlyRead.js"></script>
		<script src="./public/js/homepage/social.js"></script>
		<script src="./public/js/homepage/preference.js"></script>
	</body>
</html>
