

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-validator/2.1.7/vue-validator.min.js" integrity="sha512-9cDqG1owtZOpuOf+JYJ7GELuTjQXnmSu5bf5Wa3Z7i+7jaYWRYQxgmuWxzpKr+5hDhWGTiL1Htvrbipj0H6OAQ==" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/libraries/duDialog.min.js') ?>"></script>
<script src="<?= base_url('/assets/js/libraries/fontawesome-free-5.14.0-web/js/all.js') ?>"></script>

<script src="<?= base_url('assets/js/app.js')?>"></script>
<script src="<?= base_url('/assets/js/config.js') ?>"></script>
<script src="<?= base_url('assets/js/modules/livestream.js')?>"></script>
<script src="<?= base_url('assets/js/modules/trending.js')?>"></script>
<script src="<?= base_url('assets/js/modules/watch.js')?>"></script>
<script src="<?= base_url('assets/js/modules/channelWidget.js')?>"></script>
<script src="<?= base_url('assets/js/modules/explore.js')?>"></script>
<script src="<?= base_url('assets/js/modules/read.js')?>"></script>
<script src="<?= base_url('assets/js/modules/local.js')?>"></script>
<script src="<?= base_url('assets/js/modules/share.js')?>"></script>
<script src="<?= base_url('assets/js/modules/personalInfo.js')?>"></script>

<script src="<?= base_url('assets/js/modules/member.js')?>"></script>
<script src="<?= base_url('assets/js/modules/preference.js')?>"></script>
<script src="<?= base_url('assets/js/modules/favorite.js')?>"></script>
<script src="<?= base_url('assets/js/modules/analytic.js')?>"></script>
<script>
    var tag = document.createElement("script");
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
</script>

<style type="text/css">

	.preference {
		background: #fff;
	}
	/*#preferences{
		color: #000;
	}*/
	#preferences .videoCat h1 {
		color: #000;
	}
	#preferences .videoCat ul {
	  list-style-type: none;
	}

	#preferences .videoCat li {
	  display: inline-block;
	}

	#preferences p {
		text-align: center;
		color: #000;
	}

	#preferences .news label {
		color: #000;
	}


		#preferences .videoCat input[type="checkbox"] {
		  	display: none;
		}
		#preferences .videoCat label {
			border: 1px solid #fff;
			padding: 10px;
			display: block;
			position: relative;
			margin: 10px;
			cursor: pointer;
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
		#preferences .videoCat label::before {
			background-color: white;
			color: white;
			content: " ";
			display: block;
			border-radius: 50%;
			border: 1px solid grey;
			position: absolute;
			top: 5px;
			left: 5px;
			width: 25px;
			height: 25px;
			text-align: center;
			line-height: 28px;
			transition-duration: 0.4s;
			transform: scale(0);
		}
		#preferences .videoCat label img {
		  	height: 100px;
		  	width: 100px;
		  	transition-duration: 0.2s;
		  	transform-origin: 50% 50%;
		}

	#preferences .videoCat :checked+label {
			border-color: #ddd;
	}

	#preferences .videoCat :checked+label::before {
		content: "✓";
		background-color: #8ec300;
		transform: scale(1);
	    z-index: 990;
	}

	#preferences .videoCat :checked+label img {
		transform: scale(0.9);
		box-shadow: 0 0 5px #333;
		z-index: -1;
	}
</style>

</body>
	</body>
</html>
