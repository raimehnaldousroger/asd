<?php
	if(isset($_GET['accept-cookies'])) {
		setcookie('accept-cookies', 'true', time() + 630427, '/');
		header('Location: ./');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>NOWNa.com | Home</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="author" content="Now Corporation">
		<meta name="developers" content="Raimehn Roger, Junrel Devocion">
		<link
			rel="icon"
			type="image/png"
			href="<?= base_url('/assets/images/logo_1.png') ?>"
		/>

		<!-- custom css -->
		<link rel="stylesheet" href="<?= base_url('/assets/css/index.css') ?>">
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
		/>
		<link
			rel="stylesheet"
			href="<?= base_url('/assets/fontawesome-free-5.14.0-web/css/all.min.css') ?>"
		/>
		
		<script src="<?= base_url('assets/css/preference.css')?>"></script>
		<link rel="stylesheet" href="<?= base_url('/assets/css/duDialog.min.css') ?>" />

		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

		<!-- vue js, lodash jquery shortcode scripting, axios, toastr -->

		
		<!-- <script src="https://unpkg.com/vue@next"></script> -->
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
			integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
			crossorigin="anonymous"
		></script>
		<!-- toastr -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<link
			rel="stylesheet"
			type="text/css"
			href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"
		/>
		<style type="text/css">
			.cookie-banner {
	        	background: #ffffffbf;
			    z-index: -51;
			    padding: 20px 15px;
			    color: #000;
			    position: fixed;
			    z-index: 1000;
			    width: 100%;
			    bottom: 0px;
			}
				.cookie-banner .close {
					color: #000!important;
				}
		</style>
	</head>
	<body>
