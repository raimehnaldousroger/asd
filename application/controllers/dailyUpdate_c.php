<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dailyUpdate_c extends CI_Controller {

	function index()
	{
		$this->load->view('template/header');
		$this->load->view('daily_update');
		$this->load->view('modal/login');
		$this->load->view('template/footer');
	}
}
